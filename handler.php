<?php

require 'vendor/autoload.php';

use Google\Cloud\Storage\StorageClient;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
set_time_limit(0);
date_default_timezone_set('UTC');
session_start();

define('GUSER', 'igor@mg-app.numerologist.com'); // GMail username
define('GPWD', '8Mpsm6MjS2qHQ9Vf'); // GMail password
define ("PROCESS_SCRIPT_URL", 'https://go.numerologist.com/atv-fn-dob-email-process.php');

require_once("classes/Config.php");
require_once("classes/DB.php");
require_once("classes/AdGuru/Image/Toolbox.php");
require_once("classes/Acs/Location/Row.php");
require_once("classes/Acs/Location/Table.php");
require_once("classes/Acs/Timezone/Row.php");
require_once("classes/Acs/Timezone/Table.php");
require_once("classes/Acs/Timezone.php");
require_once("classes/NatalChart.php");
require_once("classes/AdaptedToolbox.php");
require_once('classes/PHPMailer/PHPMailerAutoload.php');
require_once('classes/mpdf/mpdf.php');


$storage = new StorageClient();
$storage->registerStreamWrapper();

if (isset($_REQUEST) && isset($_REQUEST['action'])) {

    if ($_POST['action'] == 'getLocation') {
        echo json_encode(getLocation($_POST['phrase']));
    }

    if ($_REQUEST['action'] == 'getNatalChart') {
        $birth_time_unknown = (isset($_POST['birth_time_unknown'])) ? true : false;

        $startData = [
            'current_first_name' => strval($_POST['current_first_name']),
            'current_last_name' => strval($_POST['current_last_name']),
            // 'acs_location_id' => strval($_POST['start_acs_location_id']),
            'country' => strval($_POST['country']),
            'state' => strval($_POST['state']),
            'city' => strval($_POST['city']),
            'birth_date' => array(
                'year' => strval($_POST['birth_date_year']),
                'month' => NatalChart::convertIntToStr($_POST['birth_date_month']),
                'day' => NatalChart::convertIntToStr($_POST['birth_date_day'])
            ),
            'birth_time' => [
                'hour' => NatalChart::convertIntToStr($birth_time_unknown ? '12' : $_POST['birth_time_hour']),
                'minute' => strval($birth_time_unknown ? '00' : $_POST['birth_time_minute']),
                'meridiem' => strval($birth_time_unknown ? 'AM' : $_POST['birth_time_meridiem']),
                'birth_time_unknown' => false
            ],
            "email" => $_POST['send_email_address']
        ];

        $add_to_list = true;

        // if GDPR - need to check status of the email consent checkbox too
        if( isset($_POST['is_gdpr']) && filter_var($_POST['is_gdpr'],FILTER_VALIDATE_BOOLEAN) === true && !isset($_POST['consent-checkbox'])) {
            $add_to_list = false;
        }

        if(!empty($_POST['send_email_address']) && $add_to_list === true) {
            // format date of birth
            $ts = mktime(0, 0, 0, $_POST['birth_date_month'], $_POST['birth_date_day'], $_POST['birth_date_year']);
            $dob = date('Y-m-d', $ts);

            $data = array(
                'first_name' => ucfirst($_POST['current_first_name']),
                'last_name' => ucfirst($_POST['current_last_name']),
                'email' => $_POST['send_email_address'],
                'dob' => $dob,
                'source' => $_GET['hop'],
                'offer' => 'cw-non-promo',
                'sub_offer' => 'cw lp',
                'subscribed' => true,
                'page_url' => "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
            );

            $json_data = json_encode($data);

            $curl = curl_init();

            $opts = array(
                CURLOPT_URL             => PROCESS_SCRIPT_URL,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_CUSTOMREQUEST   => 'POST',
                CURLOPT_TIMEOUT         => 10,
                CURLOPT_MAXREDIRS       => 10,
                CURLOPT_POST            => 1,
                CURLOPT_POSTFIELDS      => $json_data,
                CURLOPT_HTTPHEADER      => array('Content-Type: application/json', 'Accept: application/json', 'Content-Length: ' . strlen($json_data))
            );

            curl_setopt_array($curl, $opts);
            $response = curl_exec($curl);
            curl_close($curl);

            die($response);
        }

        $nc = new NatalChart($startData);

        $nc->getChartData();
        if (isset($_REQUEST['autosubmit'])) {
            $chart_info = json_encode($nc->outputData);
            require('views/layout.php');
        } else {
            echo json_encode([
                'outputData' => json_encode($nc->outputData)
            ]);
        }
        
    }

    //сохранение изображения с канваса на сервере
    if ($_POST['action'] == 'saveNatalChartImage') {
        $img = $_POST['imgBase64'];
        $img = str_replace('data:image/png;base64,','', $img);
        $img = str_replace(' ', '+', $img);
        $fileData = base64_decode($img);
        //saving
        $fileName = 'chart-'.time().rand(1,20).'.png';

        $contents = file_put_contents("gs://chart-images.astrology.tv/$fileName", $fileData);

        echo json_encode(array(
            'image' => $fileName,
        ));
    }

    if($_POST['action'] == 'sendMail') {
        $send_email_address = '';
        if(isset($_POST['email']) && !empty($_POST['email'])) {
            $send_email_address = $_POST['email'];
        }
        else {
            $cookies = (isset($_COOKIE['anc-details'])) ? json_decode(base64_decode($_COOKIE['anc-details'], true)) : false;
            if(!$cookies || !$cookies->send_email_address) {
                echo json_encode(array('success'=>false, 'reason' => 'no-email'));
                exit;
            } else {
                $send_email_address = $cookies->send_email_address;
            }
        }

        // изображение, полученное из POST
        $natalChartImage = $_POST['imagePath'];
        $message = str_replace('$$result_image$$', '<img width="600px" src="'.$natalChartImage.'"/>', $_SESSION['page_content']);
        $Mpdf = new mPDF('utf-8', 'A4', '10', 'Arial');
        $Mpdf->charset_in = 'utf-8';
        $Mpdf->WriteHTML($message);

        //$mailTo = $_POST['email'];
        $mailFrom = 'no-reply@mg-app.numerologist.com';
        $mailFromName = 'www.numerologist.com';
        $mailSubject = 'Your Name Numerology Report.';
        $mailError = SMTPMailer( $send_email_address, $mailFrom, $mailFromName, $mailSubject, $message, $Mpdf->Output('', 'S'), 'report.pdf' );

        echo (!$mailError) ? json_encode(array('success'=>true)) : die($mailError);
        exit;
    }
}


if(isset($_POST) && isset($_POST['action'])) {
    if($_POST['action'] == 'saveChartInPdf') {
        $natalChartImage = "https://storage.googleapis.com/chart-images.astrology.tv/". $_POST['imageContent'];
        $message = str_replace('$$result_image$$', '<img width="600px" src="'.$natalChartImage.'"/>', $_POST['pageContent']);
        $Mpdf = new mPDF('utf-8', 'A4', '10', 'Arial');
        $Mpdf->charset_in = 'utf-8';
        $Mpdf->WriteHTML($message);
        header("Content-type:application/pdf");
        echo $Mpdf->Output('', 'S');
    }
}

/**
 * Functions
 */

function getLocation($location)
{
    $locationTable = new Acs_Location_Table();
    $queryResult = $locationTable->locationSearch($location);
    
    $arr = [];
    while ($row = $queryResult->fetchArray())
        $arr[] = array("city" => $row['city'], "id" => $row['id'], "country_state" => $row['country_state'], "info" => $row['city'] . ' - ' . $row['country_state']);

    $sortArr = [];
    foreach ($arr as $item) {
        $sortArr[$item['info']] = $item;
    }
    ksort($sortArr);
    $sortArr = array_values($sortArr);
    return $sortArr;
}

function SMTPMailer( $to, $from, $fromName, $subject, $body, $attachmentString = false, $attachmentName = false ) {
    $mail = new PHPMailer(  );  // create a new object

    $mail->IsSMTP(  ); // enable SMTP
    $mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true;  // authentication enabled
    $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
    $mail->Host = 'smtp.mailgun.org';
    $mail->Port = 587;
    $mail->Username = GUSER;
    $mail->Password = GPWD;
    $mail->SetFrom( $from, $fromName );
    $mail->Subject = $subject;
    $mail->MsgHTML($body);
    $mail->IsHTML(true);
    $mail->AddAddress( $to );
    if($attachmentString)
        $mail->addStringAttachment( $attachmentString, $attachmentName, 'base64', 'application/pdf', 'attachment' );

    if( ! $mail->Send(  ) ) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return $error;
    }
}

?>