This folder contains the base image for the repository to assist in faster builds when there are no changes to the underlying docker container. The main Dockerfile in the root of the repo is only there to put the code inside the container.

Making changes to this folder should automatically build a new image.