<?php

class Acs_Location_Table
{
    /** Table name */
    protected $_name    = 'acs_location';
    protected $_primary    = 'id';
            
    public function __construct() {
    	
    }		

	public function getLocationRow( $locationID ) {	
		$query = "SELECT * FROM `$this->_name` WHERE `$this->_primary`='" . $locationID . "'";
		$db = new DB(  );
		$queryResult = $db->executeQuery( $query );
		// TODO: Change to sqlite row
		$row =  $queryResult->fetchArray();

		return (object)  $row;
	}
	
	public function locationSearch( $cityName ) {
		$db = new DB(  );
		$dbConnection = $db->getDBConnection(  );
		
		$cityName = $dbConnection->escapeString( $cityName );
		
		$query = "SELECT * FROM `$this->_name` WHERE `city` LIKE '" . $cityName . "%' LIMIT 100";
		
		$queryResult = $db->executeQuery( $query );
		return $queryResult;
	}

	public function getCityInfo( $cityID ) {
		$db = new DB(  );
		$dbConnection = $db->getDBConnection(  );
		
		$cityID = $dbConnection->escapeString( $cityID );
		
		$query = "SELECT * FROM `$this->_name` WHERE `$this->_primary`='" . $cityID . "'";
		
		$queryResult = $db->executeQuery( $query );

		return $queryResult;
	}	
}
?>