<?php
class Config {
    const APPLICATION_ENV = "production";
    const SWEPH_PATH = "library/Sweph/";
    const SWEPH_SAMPLE_PATH = "library/Sweph/sample-out.txt";
    const FONTS_PATH = "assets/fonts/";
    const TEMP_PATH = "temp/";
    # DB stuff doesn't matter we're using a sqlite db
    const HOST_NAME = "mariadb";
    const DB_USER = "root";
    const DB_PASSWORD = "";
    const DB_NAME = "atv";
    const TEMP_ICONS = 'images/icons/';
}