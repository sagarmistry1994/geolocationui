<?php
	class DB {
		protected $errorMessage = null;
		protected $mySQLi = null;
		
		public function __construct(  ) {
			
		}		
		
		public function getDBConnection(  ) {
			if( !$this->getMySQLi(  ) ) {
				try {
					$mySQLi = new SQLite3(realpath(__DIR__ ."/../db")."/location.db");
					$mySQLi->enableExceptions(true);
					$this->mySQLi = $mySQLi;
				} catch (\Exception $e) {
					$this->errorMessage = $e->getMessage();
					return false;
				}
			}
			
			return $this->getMySQLi(  );
		}		

		public function executeQuery( $query ) {
			if( !$this->getMySQLi() ) {
				if (!$this->getDBConnection()) {
					return false;
				}
			}
			
			$queryResult = $this->getMySQLi()->query($query);

			if( !$queryResult ) {
				$this->errorMessage = "Failed to execute the query!";
				
				return null;
			}

			return $queryResult;
		}
		
		public function getErrorMessage(  ) {
			return $this->errorMessage;
		}
		
		public function getMySQLi(  ) {
			return $this->mySQLi;
		}

		public function closeMySQLiConnection(  ) {
			if ($this->mySQLi) {
				return $this->mySQLi->close();
			}

			return False;
		}
	}
?>