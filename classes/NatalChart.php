<?php

class NatalChart
{
    const SE_SUN = 0;
    const SE_MOON = 1;
    const SE_MERCURY = 2;
    const SE_VENUS = 3;
    const SE_MARS = 4;
    const SE_JUPITER = 5;
    const SE_SATURN = 6;
    const SE_URANUS = 7;
    const SE_NEPTUNE = 8;
    const SE_PLUTO = 9;
    const SE_CHIRON = 10;
    const SE_LILITH = 11;
    const SE_TNODE = 12;
    const SE_POF = 13;
    const SE_VERTEX = 14;
    const LAST_PLANET = 14;

    public $signOffset = array(
        'pisces' => 0,
        'aquarius' => 30,
        'capricorn' => 60,
        'sagittarius' => 90,
        'scorpio' => 120,
        'libra' => 150,
        'virgo' => 180,
        'leo' => 210,
        'cancer' => 240,
        'gemini' => 270,
        'taurus' => 300,
        'aries' => 330,
    );

    /**
     * Set the Sign offset Array
     */
    public $signFont = [
        'aries' => 'A',
        'taurus' => 'B',
        'gemini' => 'C',
        'cancer' => 'D',
        'leo' => 'E',
        'virgo' => 'F',
        'libra' => 'G',
        'scorpio' => 'H',
        'sagittarius' => 'I',
        'capricorn' => 'J',
        'aquarius' => 'K',
        'pisces' => 'L',

        'mc (midheaven)' => 'MC',
        'ascendant' => 'Asc',
        'descendant' => 'Dsc',
        'ic' => 'IC',

        'sun' => 'Q',
        'moon' => 'R',
        'mercury' => 'S',
        'venus' => 'T',
        'mars' => 'U',
        'jupiter' => 'V',
        'saturn' => 'W',
        'uranus' => 'X',
        'neptune' => 'Y',
        'pluto' => 'Z',
        'chiron' => 't',
        'lilith' => '[',
        'true node' => '<',
        'p. of fortune' => '?',
        'vertex' => 'Vt',
    ];

    public $_hc1;

    protected $_birthDate;
    protected $_location;
    protected $_timezone;

    public $_houses;
    public $_planets;
    public $_aspects;

    public $planetCollisionLimit = 5;

    public $ignorePlanets;
    public $ignoreAspects;

    public $chartWidth = 0;
    public $chartHeight = 0;

    //внешний черный круг signRing
    protected $signRingOuterRadius; //внешний радиус
    protected $signRingInnerRadius; //внутренний радиус
    protected $signRingColor; //цвет кольца
    protected $signRingNotchRadius; //толщина кольца

    //внутренний белый круг houseRing
    protected $houseRingOuterRadius; //внешний радиус
    protected $houseRingInnerRadius; //внутренний радиус
    protected $houseRingColor; //цвет кольца
    protected $houseInnerRingColor; //цвет внутренней окружности кольца
    protected $houseRingNotchRadius; //толщина кольца

    /**
     * @var array значки АС МС
     * distance - расстояние от центра,
     * fontSize - размер шрифта в пикселах,
     * color - цвет,
     * textOffset - Расстояние от значков АС МС до цифр рядом, в градусах
     */
    protected $acMcGlyph = [];

    /**
     * @var array номера домов
     */
    protected $houseNumberGliph = [];

    /**
     * @var array иконки планет
     */
    protected $planetGlyph = [];

    /**
     * @var array знаки зодиаков
     * starSignSlice - сдвиг значка от края в градусах
     */

    protected $signGlyph = [];


    protected $planetOuterRadius;
    protected $aspectOuterRadius;
    protected $centerOuterRadius;

    protected $planetPositions;
    protected $aspectStyles;

    public $tempImagePath;
    public $swephPath;
    public $astroPath;
    public $montserratPath;
    public $oswaldPath;

    public $outputData = [];

    protected $ascendantConstantAngle = 270;
    protected $ascendantDiff;


    protected $planetAngles = [];
    protected $planetGlyphAngles = [];
    public $kSortedPlanets;
    public $userInfo;

    protected $_data = array(
        'chartwheel_type' => null,
        'current_first_name' => null,
        'current_last_name' => null,
        'birth_full_name' => null,
        'acs_location_id' => null,
        'location' => null,
        'birth_date' => null,
        'birth_time' => null,
        'email' => null,
    );

    public $planetsContent = [];
    /**
     * Measured in Degrees
     */
    protected $starSignSlice = 30;

    /**
     * NatalChart constructor.
     * @param null $data - массив с данными для расчета, введенные пользователем в форму
     */
    public function __construct($data = null)
    {
        if (!is_null($data))
            $this->setFromArray($data);

        $this->swephPath = realpath(Config::SWEPH_PATH)."/";

        $this->_birthDate = $this->getBirthDate();
        $this->_location = $this->getLocationRow();
        $this->_timezone = $this->getTimezone();
        $this->calculate();
        $this->initializeChart();
    }

    /**
     * Расчет всех необходимых данных для построения натальной карты
     *
     * @return array
     */

    public function getChartData()
    {
        $ascendantAbsoluteAngle = (isset($this->_houses) && count($this->_houses) > 0) ? $this->signOffset[strtolower($this->_houses['Ascendant']['sign'])] + (30 - AdGuru_Image_Toolbox::convertDegArrayToDec($this->_houses['Ascendant'])) : 0;
        $this->ascendantDiff = $this->ascendantConstantAngle - $ascendantAbsoluteAngle;

        foreach ($this->signOffset as $sign => $offset) {
            $newOffset = $offset + $this->ascendantDiff;
            if ($newOffset > 360) $newOffset -= 360;
            if ($newOffset <= 0) $newOffset += 360;

            $this->signOffset[$sign] = $newOffset;
        }

        foreach ($this->_planets as $planet => $details) {
            // Skip over planets we want to ignore
            if (in_array($planet, $this->ignorePlanets)) continue;
            if (!$this->_data['birth_time']['birth_time_unknown'] OR $planet != "Moon") {

                // Find the angle of the sign
                $planetRelativeAngle = AdGuru_Image_Toolbox::convertDegArrayToDec($details);
                $planetAbsoluteAngle = $this->signOffset[strtolower($details['sign'])] + ($this->starSignSlice - $planetRelativeAngle);

                // Record the Planet Positions for Drawing Aspects... Record the Position at the Outer Radius of the Aspect Circle
                $this->planetPositions[$planet]['x'] = $this->getPolarX($planetAbsoluteAngle, $this->houseRingInnerRadius);
                $this->planetPositions[$planet]['y'] = $this->getPolarY($planetAbsoluteAngle, $this->houseRingInnerRadius);

                // Record the Angles so we can later do Collision Detection and Draw the Glyphs in the right spot
                $this->planetAngles[$planet] = $planetAbsoluteAngle;
                $this->planetGlyphAngles[$planet] = $planetAbsoluteAngle;
            }
        }

        asort($this->planetGlyphAngles);
        $kSortedPlanets = [];
        foreach ($this->planetGlyphAngles as $planet => $angle) {
            $kSortedPlanets[] = ['planet' => $planet, 'angle' => $angle];
        }

        $planetsNames = [];
        $planetsAngels = [];

        // TODO: I'm fairly certain this is completely wrong
        // It is sorting the angles and the names of the planets separately
        // when it should likely be keyed by angle and sorted by the angle :/
        foreach ($kSortedPlanets as $planetKey => $planetInfo) {
            $planetName = $planetInfo["planet"];
            $planetAngel = $planetInfo["angle"];

            array_push($planetsNames, $planetName);
            array_push($planetsAngels, $planetAngel);
        }
        array_multisort(
            $planetsAngels,
            $planetsNames
        );

        $sortedPlanets = [];
        foreach ($planetsNames as $planetKey => $planetName) {
            foreach ($kSortedPlanets as $planetKey => $planetInfo) {
                $initPlanetName = $planetInfo["planet"];

                if ($initPlanetName == $planetName) {
                    $initPlanetKey = $planetKey;
                    break;
                }
            }

            array_push($sortedPlanets,
                array(
                    "planet" => $planetName,
                    "angle" => $kSortedPlanets[$initPlanetKey]["angle"]
                )
            );
        }

        $previousAngel = 0;

        foreach ($sortedPlanets as $planetKey => $planetInfo) {
            $planetName = $planetInfo["planet"];
            $currentAngel = $planetInfo["angle"];

            if (($currentAngel - $previousAngel) < $this->planetCollisionLimit) {
                $currentAngel = $previousAngel + $this->planetCollisionLimit;
            }

            switch ($planetName) {
                case 'MC':
                case 'DC':
                case 'IC':
                case 'AC':
                    unset($sortedPlanets[$planetKey]);

                default:
                    $sortedPlanets[$planetKey]["angle"] = $currentAngel;
            }

            $previousAngel = $currentAngel;
        }
        $this->kSortedPlanets = $sortedPlanets;

        //приводим контент в удобный вид и дополняем массив с контентом планет
        $apiContent = $this->getApiContent();
        $apiPlanetsArray = $apiContent['report']['detail'][0]['section'];
        
        $fullPlanetContent = [];
        $node_content = "";
        $planet_names = array_keys($this->planetsContent);
        
        foreach ($apiPlanetsArray as $apiPlanetArray) {
            $first = true;
            $planetArray = array_filter($planet_names, function($value) use ($apiPlanetArray) {
                if ($apiPlanetArray['title'] == "Nodes") {
                    $title = $apiPlanetArray['paragraph'][0]['interaspect'];
                } else {
                    $title = $apiPlanetArray['title'];
                }

                if(strpos(strtolower($title), strtolower($value)) !== false) {
                    return true;
                } 

                return false;
            });

            $planet = array_shift($planetArray);
            if (!$planet) {
                continue;
            }
            foreach ($apiPlanetArray['paragraph'] as $paragraph) {
                if ($first) {
                    $content = trim($this->planetsContent[$planet]) . ' ' . $paragraph["text"];
                    $first = false;
                } else {
                    $content = $paragraph["text"];
                }
                
                $fullPlanetContent[$planet] = [
                    "content" => $content,
                    "heading" =>  $paragraph['interaspect'],
                    "sign" => $planet
                ];
                break; // For now we only want the first paragraph
            }
        }

        $outputData = [
            'chartWidth' => $this->chartWidth,
            'chartHeight' => $this->chartHeight,

            'signRingOuterRadius' => $this->signRingOuterRadius,
            'signRingInnerRadius' => $this->signRingInnerRadius,
            'signRingColor' => $this->signRingColor,
            'signRingNotchRadius' => $this->signRingNotchRadius,

            'houseRingOuterRadius' => $this->houseRingOuterRadius,
            'houseRingInnerRadius' => $this->houseRingInnerRadius,
            'houseRingColor' => $this->houseRingColor,
            'houseInnerRingColor' => $this->houseInnerRingColor,
            'houseRingNotchRadius' => $this->houseRingNotchRadius,

            'signOffset' => $this->signOffset,
            'signFont' => $this->signFont,
            'ascendantDiff' => $this->ascendantDiff,

            'aspects' => $this->_aspects,
            'ignoreAspects' => $this->ignoreAspects,
            'planetPositions' => $this->planetPositions,
            'aspectStyles' => $this->aspectStyles,

            'houses' => $this->_houses,

            'hc1' => $this->_hc1,

            'kSortedPlanets' => $this->kSortedPlanets,
            'planets' => $this->_planets,

            'acMcGlyph' => $this->acMcGlyph,
            'houseNumberGliph' => $this->houseNumberGliph,
            'planetGlyph' => $this->planetGlyph,
            'signGlyph' => $this->signGlyph,
            'planetsContent' => $fullPlanetContent,
            'userInfo' => $this->userInfo,
        ];
        $this->outputData = $outputData;
    }

    public function calculate()
    {

        $month = $this->getBirthDate('m');
        $day = $this->getBirthDate('j');
        $year = $this->getBirthDate('Y');

        $hour = $this->getBirthDate('H');
        $minute = $this->getBirthDate('i');

        // Need to add -1 or +1 depending on DST.
        $timezone = $this->getTimezone()->getBase() / 900;
        $timezone = (($this->getTimezone()->getType() == 1) && ($this->getTimezone()->getBase() < 0) ? $timezone - 1 : $timezone);

        // Get Longitude Values
        $longitudeArray =[];
        preg_match('/([0-9]*)(E|W)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->longitude, $longitudeArray);

        $long_deg = intval($longitudeArray[1]);
        $long_min = intval($longitudeArray[3]);
        $ew = ($longitudeArray[2] == 'E' ? 1 : -1);

        // Get Latitude Values
        $latitudeArray =[];
        preg_match('/([0-9]*)(N|S)([0-9]*)\'([0-9]*)/', $this->getLocationRow()->latitude, $latitudeArray);

        $lat_deg = intval($latitudeArray[1]);
        $lat_min = intval($latitudeArray[3]);
        $ns = ($latitudeArray[2] == 'N' ? 1 : -1);

        //assign data from database to local variables
        $inmonth = $month;
        $inday = $day;
        $inyear = $year;

        $inhours = $hour;
        $inmins = $minute;
        $insecs = "0";

        $intz = $timezone * -1;

        $my_longitude = $ew * ($long_deg + ($long_min / 60));
        $my_latitude = $ns * ($lat_deg + ($lat_min / 60));

        if ($intz >= 0) {
            $whole = floor($intz);
            $fraction = $intz - floor($intz);
        } else {
            $whole = ceil($intz);
            $fraction = $intz - ceil($intz);
        }

        $inhours = $inhours - $whole;
        $inmins = $inmins - ($fraction * 60);


        // adjust date and time for minus hour due to time zone taking the hour negative
        $utdatenow = strftime("%d.%m.%Y", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));
        $utnow = strftime("%H:%M:%S", mktime($inhours, $inmins, $insecs, $inmonth, $inday, $inyear));

        // get LAST_PLANET planets and all house cusps
        $h_sys = "p";

        if (Config::APPLICATION_ENV != 'production') {
            $out = file(Config::SWEPH_SAMPLE_PATH);
        } else {
            $toRun = $this->swephPath . "swetest -edir" . $this->swephPath . " -b$utdatenow -ut$utnow -p0123456789DAttt -eswe -house$my_longitude,$my_latitude,$h_sys -flsj -g, -head";
            exec($toRun, $out, $ret);
        }

        // Each line of output data from swetest is exploded into array $row, giving these elements:
        // 0 = longitude
        // 1 = speed
        // 2 = house position
        // planets are index 0 - index (LAST_PLANET), house cusps are index (LAST_PLANET + 1) - (LAST_PLANET + 12)
        $longitude1 =[];
        $speed1 =[];
        $house_pos1 =[];
        foreach ($out as $key => $line) {
            $row = explode(',', $line);

            $longitude1[$key] = floatval($row[0]);
            $speed1[$key] = floatval($row[1]);
            $house_pos1[$key] = (isset($row[2]) ? floatval($row[2]) : 0);
        }

        $pl_name =[];
        $pl_name[0] = "Sun";
        $pl_name[1] = "Moon";
        $pl_name[2] = "Mercury";
        $pl_name[3] = "Venus";
        $pl_name[4] = "Mars";
        $pl_name[5] = "Jupiter";
        $pl_name[6] = "Saturn";
        $pl_name[7] = "Uranus";
        $pl_name[8] = "Neptune";
        $pl_name[9] = "Pluto";
        $pl_name[10] = "Chiron";
        $pl_name[11] = "Lilith";
        $pl_name[12] = "True Node";
        $pl_name[13] = "P. of Fortune";        //add a planet
        $pl_name[14] = "Vertex";
        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "Midheaven";

        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "House 2";
        $pl_name[self::LAST_PLANET + 3] = "House 3";
        $pl_name[self::LAST_PLANET + 4] = "House 4";
        $pl_name[self::LAST_PLANET + 5] = "House 5";
        $pl_name[self::LAST_PLANET + 6] = "House 6";
        $pl_name[self::LAST_PLANET + 7] = "House 7";
        $pl_name[self::LAST_PLANET + 8] = "House 8";
        $pl_name[self::LAST_PLANET + 9] = "House 9";
        $pl_name[self::LAST_PLANET + 10] = "MC (Midheaven)";
        $pl_name[self::LAST_PLANET + 11] = "House 11";
        $pl_name[self::LAST_PLANET + 12] = "House 12";

        //calculate the Part of Fortune
        //is this a day chart or a night chart?
        if ($longitude1[self::LAST_PLANET + 1] > $longitude1[self::LAST_PLANET + 7]) {

            if ($longitude1[0] <= $longitude1[self::LAST_PLANET + 1] && $longitude1[0] > $longitude1[self::LAST_PLANET + 7]) {
                $day_chart = true;
            } else {
                $day_chart = false;
            }

        } else {

            if ($longitude1[0] > $longitude1[self::LAST_PLANET + 1] && $longitude1[0] <= $longitude1[self::LAST_PLANET + 7]) {
                $day_chart = false;
            } else {
                $day_chart = true;
            }

        }

        if ($day_chart == true) {
            $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] + $longitude1[1] - $longitude1[0];
        } else {
            $longitude1[self::SE_POF] = $longitude1[self::LAST_PLANET + 1] - $longitude1[1] + $longitude1[0];
        }

        if ($longitude1[self::SE_POF] >= 360) {
            $longitude1[self::SE_POF] = $longitude1[self::SE_POF] - 360;
        }

        if ($longitude1[self::SE_POF] < 0) {
            $longitude1[self::SE_POF] = $longitude1[self::SE_POF] + 360;
        }

        //capture the Vertex longitude
        $longitude1[self::LAST_PLANET] = $longitude1[self::LAST_PLANET + 16];        //Asc = +13, MC = +14, RAMC = +15, Vertex = +16


        //get house positions of planets here
        for ($x = 1; $x <= 12; $x++) {

            for ($y = 0; $y <= self::LAST_PLANET; $y++) {

                $pl = $longitude1[$y] + (1 / 36000);

                if ($x < 12 && $longitude1[$x + self::LAST_PLANET] > $longitude1[$x + self::LAST_PLANET + 1]) {

                    if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[$x + self::LAST_PLANET + 1] && $pl >= 0)) {

                        $house_pos1[$y] = $x;
                        continue;

                    }

                }

                if ($x == 12 && ($longitude1[$x + self::LAST_PLANET] > $longitude1[self::LAST_PLANET + 1])) {

                    if (($pl >= $longitude1[$x + self::LAST_PLANET] && $pl < 360) || ($pl < $longitude1[self::LAST_PLANET + 1] && $pl >= 0)) {
                        $house_pos1[$y] = $x;
                    }

                    continue;
                }

                if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[$x + self::LAST_PLANET + 1]) && ($x < 12)) {

                    $house_pos1[$y] = $x;
                    continue;
                }

                if (($pl >= $longitude1[$x + self::LAST_PLANET]) && ($pl < $longitude1[self::LAST_PLANET + 1]) && ($x == 12)) {
                    $house_pos1[$y] = $x;
                }
            }
        }

        //sk65col
        $ubt1 = ($this->_data['birth_time']['birth_time_unknown']) ? 1 : 0;

        $rx1 = "";
        for ($i = 0; $i <= self::SE_TNODE; $i++) {

            if ($speed1[$i] < 0) {
                $rx1 .= "R";
            } else {
                $rx1 .= " ";
            }
        }

        for ($i = 1; $i <= self::LAST_PLANET; $i++) {
            $hc1[$i] = $longitude1[self::LAST_PLANET + $i];
        }

        if ($ubt1 == 1) {
            $a1 = self::SE_TNODE;
        } else {
            $a1 = self::LAST_PLANET;
        }

        $planets =[];

        for ($i = 0; $i <= $a1; $i++) {

            if ($ubt1 == 1) {
                $hse = "&nbsp;";
            } else {
                $hse = floor($house_pos1[$i]);
            }

            $planets[$pl_name[$i]] = array_merge($this->convertLongitudeToArray($longitude1[$i]), array('mid' => $this->mid($rx1, $i + 1, 1), 'house' => $hse));
        }

        $houses =[];
        if ($ubt1 == 0) {

            for ($i = self::LAST_PLANET + 1; $i <= self::LAST_PLANET + 12; $i++) {

                if ($i == self::LAST_PLANET + 1) {
                    $name = 'Ascendant';
                } elseif ($i == self::LAST_PLANET + 10) {
                    $name = 'MC (Midheaven)';
                } else {
                    $name = 'House ' . ($i - self::LAST_PLANET);
                }

                $houses[$name] = $this->convertLongitudeToArray($longitude1[$i]);
            }
        }

        $aspects =[];

        // include Ascendant and MC
        $longitude1[self::LAST_PLANET + 1] = $hc1[1];
        $longitude1[self::LAST_PLANET + 2] = $hc1[10];

        $pl_name[self::LAST_PLANET + 1] = "Ascendant";
        $pl_name[self::LAST_PLANET + 2] = "Midheaven";

        if ($ubt1 == 1) {
            $a1 = self::SE_TNODE;
        } else {
            $a1 = self::LAST_PLANET + 2;
        }

        $asp_name[1] = "conjunction";
        $asp_name[2] = "opposition";
        $asp_name[3] = "trine";
        $asp_name[4] = "square";
        $asp_name[5] = "quincunx";
        $asp_name[6] = "sextile";
        /**
         * Aspects added by Ant
         */
        $asp_name[7] = 'semisquare';
        $asp_name[8] = 'semisextile';
        $asp_name[9] = 'sesquiquare';
        $asp_name[10] = 'quintile';
        $asp_name[11] = 'biquintile';

        /**
         * Calculate Aspects
         */
        for ($i = 0; $i <= $a1; $i++) {

            for ($j = 0; $j <= $a1; $j++) {

                $q = 0;
                $da = abs($longitude1[$i] - $longitude1[$j]);

                if ($da > 180) {
                    $da = 360 - $da;
                }

                // set orb - 8 if Sun or Moon, 6 if not Sun or Moon
                if ($i == 0 Or $i == 1 Or $j == 0 Or $j == 1) {
                    $orb = 8;
                } else {
                    $orb = 6;
                }

                // is there an aspect within orb?
                if ($da <= $orb) {
                    $q = 1;
                } elseif (($da <= (60 + $orb)) And ($da >= (60 - $orb))) {
                    $q = 6;
                } elseif (($da <= (90 + $orb)) And ($da >= (90 - $orb))) {
                    $q = 4;
                } elseif (($da <= (120 + $orb)) And ($da >= (120 - $orb))) {
                    $q = 3;
                } elseif (($da <= (150 + $orb)) And ($da >= (150 - $orb))) {
                    $q = 5;
                } elseif ($da >= (180 - $orb)) {
                    $q = 2;
                }

                /**
                 * Add aspects to the aspects array
                 *
                 * Record Planet 1, Aspect, Planet 2 and the Orb.
                 */
                if ($q > 0 And $i != $j) {
                    $aspects[] = array('Planet1' => $pl_name[$i], 'Aspect' => ucwords($asp_name[$q]), 'Planet2' => $pl_name[$j], 'Orb' => sprintf("%.2f", abs($da)));
                }
            }
        }

        $this->_planets = $planets;
        $this->_houses = $houses;
        $this->_aspects = $aspects;
        $this->_hc1 = $hc1;
    }


    public function getApiContent()
    {
        $apiUrlParams = [
            'ServerURL' => 'http://www.zdki.us',
            'Uristem' => 'taReportsw/MakeReport.aspx',
        ];
        $apiGetParams = [
            'ReportID' => '41.1',
            'ReportVariation' => 'sho',
            'ReportFormat' => 'JSON',
            'Name1' => $this->_data['current_first_name'] . '+' . $this->_data['current_last_name'],
            'BirthDate1' => date('m/d/Y', mktime(0, 0, 0, $this->_data['birth_date']['month'], $this->_data['birth_date']['day'], $this->_data['birth_date']['year'])),//implode date with / f.e 12/18/1963
            'Name2' => '',
            'BirthDate2' => '',
            'StartDate' => '',
            'Length' => '',
            'AccountID' => 'numerologist',
            'AppID' => 'CDS',
            'MemberID' => '1234567890',
            'V' => '2',
        ];
        $apiGetUrl = [];
        foreach ($apiGetParams as $key => $value) {
            $apiGetUrl[] = $key . '=' . $value;
        }

        $contentApiUrl = implode('/', $apiUrlParams) . '?' . implode('&', $apiGetUrl);
        return $contentApi = json_decode(file_get_contents($contentApiUrl), true);
    }

    /**
     * Инициализация параметров для построения карты (цвета фонов, размеры кругов и тд.)
     *
     * @return void
     */
    public function initializeChart()
    {
        $this->chartWidth = 1200;
        $this->chartHeight = $this->chartWidth;
        $margin = 105;

        $this->astroPath = Config::FONTS_PATH . 'astro/Astro.ttf';
        $this->montserratPath = Config::FONTS_PATH . 'montserrat/montserrat.ttf';
        $this->oswaldPath = Config::FONTS_PATH . 'oswald/Oswaldregular.ttf';
        $this->tempImagePath = Config::TEMP_PATH;

        $this->signRingNotchRadius = 100;
        $this->signRingOuterRadius = $this->chartWidth / 2 - $margin;
        $this->signRingInnerRadius = $this->signRingOuterRadius - $this->signRingNotchRadius;
        $this->signRingColor = "#2e302e";

        $this->houseRingNotchRadius = 170;
        $this->houseRingOuterRadius = $this->signRingInnerRadius;
        $this->houseRingInnerRadius = $this->houseRingOuterRadius - $this->houseRingNotchRadius;
        $this->houseRingColor = "#ffffff";
        $this->houseInnerRingColor = "#bdbebe";

        $this->ignorePlanets = array('Lilith', 'P. of Fortune', 'Vertex');
        $this->ignoreAspects = array('Lilith', 'P. of Fortune', 'Vertex');

        $this->aspectStyles = [
            'Square' => ['style' => 'line', 'color' => ['R' => 0xFF, 'G' => 0x00, 'B' => 0x00]],
            'Trine' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0xFF]],
            'Conjunction' => ['style' => 'line', 'color' => ['R' => 0xFF, 'G' => 0x00, 'B' => 0x00]],
            'Quincunx' => ['style' => 'dotted', 'color' => ['R' => 0x00, 'G' => 0xFF, 'B' => 0x00]],
            'Sextile' => ['style' => 'dotted', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0xFF]],
            'Semisextile' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0xFF]],
            'Semisquare' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0x00]],
            'Opposition' => ['style' => 'dotted', 'color' => ['R' => 0xFF, 'G' => 0x00, 'B' => 0x00]],
            'Sesquiquare' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0xFF, 'B' => 0x00]],
            'Quintile' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0x00]],
            'Biquintile' => ['style' => 'line', 'color' => ['R' => 0x00, 'G' => 0x00, 'B' => 0x00]],
        ];
        foreach ($this->aspectStyles as &$aspectStyles) {
            $aspectStyles['color'] = ['R' => 46, 'G' => 48, 'B' => 48];
        }

        $houseRingMargin = 40;

        $this->acMcGlyph = [
            'distance' => $this->signRingOuterRadius + $margin / 3 + 5,
            'distance_1' => $this->signRingOuterRadius + $margin / 2 + 1,
            'fontSize' => 30,
            'fontSizeNum' => 15,
            'color' => '#2e302e',
            'textOffset' => 4,
            'textOffset_1' => 3,
        ];

        $this->houseNumberGliph = [
            'distance' => $this->houseRingInnerRadius + 20,
            'fontSize' => 20,
            'color' => '#2e302e',
        ];

        $this->planetGlyph = [
            'distance' => $this->houseRingOuterRadius - $houseRingMargin,
            'fontSize' => 60,
            'color' => '#2e302e',
            'line' => 10,
        ];

        $this->signGlyph = [
            'distance' => $this->signRingOuterRadius - $this->signRingNotchRadius / 2,
            'fontSize' => 75,
            'color' => '#ffffff',
            'starSignSlice' => $this->starSignSlice / 2,
        ];

        $this->planetsContent = [
            'sun' => 'The Sun is the core of you - what makes you tick. This is the inner fire that drives you, the energy that is the basis of your personality.',
            'moon' => 'Are you happy-go-lucky or do you get mad fast? Do you talk about your feelings or keep them bottled up inside? The Moon knows the answer.',
            'mercury' => 'Do you talk faster than your brain moves, or are you a slower, more thoughtful type? Mercury shows how you think, talk and communicate.',
            'venus' => 'Venus is the planet of sweet lovin’! It indicates whether you\'re a fun, spontaneous lover or a sweet, thoughtful, hearts and flowers type.',
            'mars' => 'Mars rules your inner drive. How do you pursue a promotion or approach that hottie? The action planet speaks to expressing your passions.',
            'jupiter' => 'Jupiter speaks to your higher mind and your potential for inner growth. It governs your spirituality and optimism; it’s your lucky star.',
            'saturn' => 'Saturn is the great reality check. As your sense of duty, it looks over your shoulder and tells you to focus, work hard and get serious.',
            'uranus' => 'Uranus is so much fun but it can mess with your mind! It rules spontaneity and the part of you that is new, fresh, raw and unexpected.',
            'neptune' => 'Like a dream, Neptune can be wild and wonderful, poignant and sweet or just confusing. Reality can be hard to see through your imagination.',
            'pluto' => 'Pluto rules your true grit and the deep, subtle urges that drive you from within. This intensely powerful planet is not to be trifled with!',
            'north node' => 'The North Node represents your destiny and your future while the opposite point - the South Node is your past or past life issues you bring into this lifetime.',
            'south node' => '',
        ];

        $formatDate = date('d M Y', mktime(0, 0, 0, $this->_data['birth_date']['month'], $this->_data['birth_date']['day'], $this->_data['birth_date']['year']));
        $formatName = $this->_data['current_first_name'] . ' ' . $this->_data['current_last_name'];
        $zodiac = strtolower(self::getZodiac($this->_data['birth_date']['month'], $this->_data['birth_date']['day']));
        $this->userInfo = [
            'title' => [
                'text' => 'BIRTH CHART',
                'fontSize' => self::getTextFontSize('BIRTH CHART', 500, 55, $this->montserratPath),
            ],
            'name' => [
                'text' => $formatName,
                'fontSize' => self::getTextFontSize($formatName, 400, 45, $this->oswaldPath),
            ],
            'date' => [
                'text' => $formatDate,
                'fontSize' => self::getTextFontSize($formatDate, 300, 45, $this->oswaldPath),
            ],
            'address' => [
                'text' => $this->_location->city,
                'fontSize' => self::getTextFontSize($this->_location->city, 200, 45, $this->oswaldPath),
            ],
            "email" => $this->_data['email'],
            'background' => 'assets/images/' . $zodiac . '-background.jpg',
            'backgroundInner' => 'assets/images/chart-wheel-background/' . $zodiac . '.png',
            "zodiacName" => $zodiac
        ];
    }

    public static function getTextFontSize($text, $width, $defautltFontSize, $fontFamily) {

        $textBox = imagettfbbox($defautltFontSize,0,$fontFamily, $text);
        $textWidth = abs($textBox[4] - $textBox[0]);

        if($textWidth > $width) {
            $defautltFontSize--;
            return self::getTextFontSize($text, $width, $defautltFontSize, $fontFamily);
        }
        return $defautltFontSize;
    }

    public function mid($midstring, $midstart, $midlength)
    {
        return (substr($midstring, $midstart - 1, $midlength));
    }

    public static function convertIntToStr($integer)
    {
        $string = ( string )$integer;

        $firstCharacter = $string[0];
        $zeroCharacter = "0";

        if ((strlen($string) == 1) and ($firstCharacter != $zeroCharacter)) {
            $string = "0" . $string;
        }

        return $string;
    }

    public Function convertLongitudeToArray($longitude)
    {
        $signs = ['Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces'];

        $sign_num = floor($longitude / 30);
        $pos_in_sign = $longitude - ($sign_num * 30);
        $deg = floor($pos_in_sign);
        $full_min = ($pos_in_sign - $deg) * 60;
        $min = floor($full_min);
        $full_sec = round(($full_min - $min) * 60);

        if ($deg < 10) {
            $deg = "0" . $deg;
        }

        if ($min < 10) {
            $min = "0" . $min;
        }

        if ($full_sec < 10) {
            $full_sec = "0" . $full_sec;
        }

        return array('deg' => $deg, 'sign' => $signs[$sign_num], 'min' => $min, 'sec' => $full_sec);
    }

    /**
     * Sets all data from an array.
     *
     * @param  array $data
     */
    public function setFromArray(array $data)
    {
        $defaults = array_combine(array_keys($this->_data), array_fill(0, count($this->_data), null));
        $data = array_intersect_key($data, $defaults);

        foreach ($data as $columnName => $value) {
            $this->__set($columnName, $value);
        }

    }

    public function __set($columnName, $value)
    {
        if (!array_key_exists($columnName, $this->_data)) {
            throw new Exception("Specified column \"$columnName\" is not in the row");
        }
        $this->_data[$columnName] = $value;
    }

    /**
     * Return Birth Date
     */
    public function getBirthDate($format = null)
    {
        if (!($this->_birthDate instanceof DateTime) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date'])) && (isset($this->_data['birth_time']) && !empty($this->_data['birth_time']))) {
            $this->_birthDate = new DateTime($this->_data['birth_date']['year'] . '-' . $this->_data['birth_date']['month'] . '-' . $this->_data['birth_date']['day'] . ' ' . $this->_data['birth_time']['hour'] . ':' . $this->_data['birth_time']['minute'] . ' ' . $this->_data['birth_time']['meridiem']);
        } else if (!($this->_birthDate instanceof DateTime) && (isset($this->_data['birth_date']) && !empty($this->_data['birth_date']))) {
            $this->_birthDate = new DateTime($this->_data['birth_date']);
        }

        if ($this->_birthDate instanceof DateTime) {

            if (is_null($format))
                return $this->_birthDate;

            return $this->_birthDate->format($format);
        }
        return false;
    }

    /**
     * Return Location
     *
     * @return Acs_Location_Row
     */
    public function getLocationRow()
    {

        if (!$this->_location) {

            $locationTable = new Acs_Location_Table();
            $row = $locationTable->getLocationRow($this->_data['acs_location_id']);

            if ($row)
                $this->_location = $row;
        }

        if ($this->_location)
            return $this->_location;

        return null;

    }

    /**
     * Return Timezone
     *
     * @return Acs_Timezone
     */

    public function getTimezone()
    {

        if (!$this->_timezone instanceof Acs_Timezone) {

            $tz = new Acs_Timezone($this->getLocationRow(), $this->getBirthDate());

            if ($tz instanceof Acs_Timezone)
                $this->_timezone = $tz;
        }

        if ($this->_timezone instanceof Acs_Timezone)
            return $this->_timezone;

        return null;

    }


    function getPolarX($angle, $distance)
    {
        $result = $this->chartWidth / 2 + sin(deg2rad($angle)) * $distance;

        return $result;

    }

    function getPolarY($angle, $distance)
    {
        $result = $this->chartWidth / 2 + cos(deg2rad($angle + 180)) * $distance;

        return $result;
    }

    /**
     * Find a Zodiac Sign by Birthdate
     *
     * @param  mixed $birthdate $year-$month-$day.
     * @return string
     */
    public static function getZodiac($month, $day)
    {

        if (($month == 3 && $day > 20) || ($month == 4 && $day < 20)) {
            $zodiac = "Aries";
        } elseif (($month == 4 && $day > 19) || ($month == 5 && $day < 21)) {
            $zodiac = "Taurus";
        } elseif (($month == 5 && $day > 20) || ($month == 6 && $day < 21)) {
            $zodiac = "Gemini";
        } elseif (($month == 6 && $day > 20) || ($month == 7 && $day < 23)) {
            $zodiac = "Cancer";
        } elseif (($month == 7 && $day > 22) || ($month == 8 && $day < 23)) {
            $zodiac = "Leo";
        } elseif (($month == 8 && $day > 22) || ($month == 9 && $day < 23)) {
            $zodiac = "Virgo";
        } elseif (($month == 9 && $day > 22) || ($month == 10 && $day < 23)) {
            $zodiac = "Libra";
        } elseif (($month == 10 && $day > 22) || ($month == 11 && $day < 22)) {
            $zodiac = "Scorpio";
        } elseif (($month == 11 && $day > 21) || ($month == 12 && $day < 22)) {
            $zodiac = "Sagittarius";
        } elseif (($month == 12 && $day > 21) || ($month == 1 && $day < 20)) {
            $zodiac = "Capricorn";
        } elseif (($month == 1 && $day > 19) || ($month == 2 && $day < 19)) {
            $zodiac = "Aquarius";
        } elseif (($month == 2 && $day > 18) || ($month == 3 && $day < 21)) {
            $zodiac = "Pisces";
        }

        return $zodiac;
    }
}

?>