#!/bin/bash

if [ -z "$DOCKER_HOST" ]; then
    if which docker-machine > /dev/null; then
        eval "$(docker-machine env)"
    fi
fi

if [ "$1" == "build" ]; then
    docker build --build-arg WWW_DATA_UID=$(id -u) -t gcr.io/astrology-tv/chart-wheel-runner:master Container/.
    docker build -t chart-wheel .
fi
docker rm -f chart-wheel
docker run -e SKIP_LOCKDOWN_DOCUMENT_ROOT=true -p 32768:8080 -v "/${PWD#*/}":/app --privileged --name chart-wheel chart-wheel
