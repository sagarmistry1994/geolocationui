<div class="hidden">
    <div id="send-email-popup" class="mfp-hide">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-4">
                    <form id="send-email-form">
                        <div class="input-group">
                            <input id="send-email-address" name="send-email-address" type="email" class="form-control"
                                   placeholder="Email" required>
                            <div class="input-group-btn">
                                <button id="send-email-submit" type="button" class="btn btn-success">Send Email</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>