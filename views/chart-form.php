<!DOCTYPE html>
<html>
	<head>
		
	<link rel="stylesheet"
          href="/assets/css/easy-autocomplete.min.css"/>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>
    
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/chartwheel-form.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800,900&display=swap" rel="stylesheet">	
		<!-- Google Tag Manager -->
	    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	    })(window,document,'script','dataLayer','GTM-MHBL97K');</script>
	    <!-- End Google Tag Manager -->
	</head>
	<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHBL97K"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<div class="container-fluid" id="top_bg">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<div align="center"><img id="logo_top" src="/assets/images/chart-wheel-form/gold_logo.png"></div>
					
					<div align="center"><h1 id="headline">Are You Really Who You Think You Are?</h1></div>
					
					<div align="center"><h3 id="sub_headline">Reveal Hidden Aspects of Your Highest Self with Your 100% free Astrological Birth Chart.</h3></div>
					
					<div align="center"><div class="form_wrapper">
												
						<div class="form">
							
							<form id="natal-chart-form" autocomplete="off" action="/handler.php?action=getNatalChart&autosubmit=true" method="post">

                            <input type="hidden" name="is_gdpr" value="false" />
						
                            <input type="text" name="current_first_name" id="current_first_name" class="form_input half left" placeholder="Your First Name"
                            value="<?php echo ($cookies) ? $cookies->current_first_name : $_POST['current_first_name']; ?>">
							
                            <input type="text" name="current_last_name" id="current_last_name" class="form_input half right" placeholder="Your Last Name"
                            value="<?php echo ($cookies) ? $cookies->current_last_name : $_POST['current_last_name']; ?>">
							
							<select name="birth_date_month" class="form_input form_select" id="birth_date_month">
								
                            <option label="Month" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Month</option>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_date_month == $i) || $_POST['birth_date_month'] == "$i") ? 'selected' : ''; ?>><?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?></option>
                            <?php endfor; ?>
								
							</select>
							
							<select name="birth_date_day" class="form_input form_select" id="birth_date_day">
								
                            <option label="Day" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Day</option>
                            <?php for ($i = 1; $i <= 31; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_date_day == $i) || $_POST['birth_date_day'] == "$i") ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
								
							</select>
							
							<select name="birth_date_year" class="form_input form_select" id="birth_date_year">
								
                            <option label="Year" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Year</option>
                            <?php
                            // Setup default year as 1970
                            if (!$cookies || !$cookies->birth_date_year > (date('Y') - 100)) {
                                $defaultDate = 1970;
                            } else {
                                $defaultDate = (int) $cookies->birth_date_year;
                            }
                            
                            for ($i = date('Y') - 100; $i <= date('Y') - 5; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo ($defaultDate === $i|| $_POST['birth_date_year'] == "$i") ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
							
							</select>
							
							<div id="time_birth">
							
								<select name="birth_time_hour" class="form_input form_select" id="birth_time_hour">
									
                                <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_time_hour == $i)|| $_POST['birth_time_hour'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 12) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
									
								</select>
								
								<select name="birth_time_minute" class="form_input form_select" id="birth_time_minute">
									
                                <?php for ($i = 0; $i <= 59; $i++): ?>
                                <option label="<?php echo sprintf("%02d", $i); ?>"
                                        value="<?php echo sprintf("%02d", $i); ?>" <?php echo (($cookies && $cookies->birth_time_minute == $i)|| $_POST['birth_time_minute'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 0) ? 'selected' : ''; ?>><?php echo sprintf("%02d", $i); ?></option>
                            <?php endfor; ?>
								
								</select>
							
								<select name="birth_time_meridiem" class="form_input form_select" id="birth_time_meridiem">
									
                                <option label="a.m."
                                    value="AM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'AM')|| $_POST['birth_time_meridiem'] == "AM") ? 'selected' : ''; ?>>
                                a.m.
                            </option>
                            <option label="p.m."
                                    value="PM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'PM')|| $_POST['birth_time_meridiem'] == "PM") ? 'selected' : ''; ?> <?php echo (!$cookies) ? 'selected' : ''; ?>>
                                p.m.
                            </option>
								
								</select>
							
							</div>
							
							<div style="clear: both; height: 10px;"></div>
							
								<input type="checkbox" name="birth_time_unknown" value="1" class="form_checkbox" id="birth_time_unknown"><span class="form_checkbox_label">I do not know my time of birth</span>
							
							<div style="clear: both; height: 1px;"></div>
							
							<div class="locationwrap">
							<p>City of Birth</p>
							<!-- Geolocation -->
							<select name="country" class="form_input form_select countries" id="countryId">
							<option value="">Select Country</option>
							</select>
							<select name="state" class="form_input form_select states" id="stateId" required="required">
							<option value="">Select State</option>
							</select>
							<select name="city" class="form_input form_select cities" id="cityId" required="required">
							<option value="">Select City</option>
							</select>
							<!-- Geolocation -->
							</div>
							<!-- <input type="text" name="start_city_search" class="form_input" placeholder="City of Birth" id="birth_city_input" autocomplete="off" > -->
																
                            <input type="text" name="send_email_address" class="form_input" id="send_email_address" placeholder="Email" value="<?php echo ($cookies) ? isset($cookies->send_email_address) ? $cookies->send_email_address : $_POST['send_email_address'] : $_POST['send_email_address']; ?>">	
							
							

                            <!-- GDPR content -->
        					<div class="gdpr-email-consent">
								
								<p class="form_disclaimer">Please confirm you’d like to receive email communications from Astrology.tv. You can update your email preferences at anytime. <a href="https://astrology.tv/privacy-policy/" target="_blank">Privacy&nbsp;Policy</a></p>
								
								<div style="clear: both; height: 10px;"></div>

								<input type="checkbox" id="consent-checkbox" name="consent-checkbox" value="1" class="form_checkbox"><span class="form_checkbox_label">Yes! I want to receive emails, forecasts and personalized readings.</span>

							</div>
								
							<div style="clear: both; height: 1px;"></div>
							
							<div align="center"><input type="image" class="mb_form_submit" src="/assets/images/chart-wheel-form/mb_top_form_button.png"></div>
							
							<div align="center"><input type="image" class="dt_form_submit" src="/assets/images/chart-wheel-form/dt_top_form_button.png"></div>

							<div class="form_disclaimer"><em>We use your information to deliver a personalized, accurate astrology reading. We will only use your email address to occasionally send you newsletters or announcements about cool stuff we think you’ll like, but we make it easy for you to unsubscribe from any of our mailings at any time. We will never spam you or sell your information.</em> <span class="privacy_link"><a href="https://astrology.tv/privacy-policy/" target="_blank">Privacy&nbsp;Policy</a></span></div>
							
							<input type="hidden" id="start_acs_location_id"
                               value=""
                               name="start_acs_location_id">
							<input type="hidden" name="autosubmit" value="true">
						
                                	
						
						</form>
                                
                                </div>
						
					</div>	
					
					<img src="/assets/images/chart-wheel-form/dt_top_form_shadow.png" id="shadow">
					
					</div>	
					
				</div>
				
			</div>			
			
		</div>	
		
		<div class="container" id="copy_container">
			
			<div class="row">
				
				<div class="col-md-5">
					
					<div align="center"><img src="/assets/images/chart-wheel-form/dt_planet.png" id="planet_image"></div>
					
				</div>
				
				<div class="col-md-7">
					
					<p id="copy_headline">You're a Unique Being, with a Unique Soul</p>
					
					<p id="copy_subheadline">More than a Sun Sign</p>
					
					<p id="copy">Hey baby, what's your sign? Cheesy chat-up lines aside, like most people you probably already know your zodiac sign and a bit about what it means. But your sign is just one very small piece of your hugely complex personal birth chart, and every single zodiac sign plays a part in your astrological makeup.<br /><br />

The Sun, the Moon and all of the planets were in an exact location in the sky at the time and place of your birth. Your birth chart is the essence of your personality, a key to understanding what makes YOU, a blueprint to your soul...</p>
					
				</div>		
				
			</div>		
			
		</div>	
		
	</div>
	
	<div class="container-fluid" id="bottom_bg">
		
		<div class="container" id="bullets_container">
		
			<div class="row">
				
				<div class="col-md-12">
					
					<p id="bullets_headline">In your Free Personalized Astrology Birth Chart, We'll Calculate the Exact Makeup of the Heavens at Your Time and Date of Birth to Reveal...</p>
					
				</div>	
				
			</div>	
			
			<div class="row">
				
				<div class="col-md-6">
					
					<ul class="bullets bullets_left">
						
						<li>The location of all of the planets in relation to the zodiac signs and the houses of the natal chart.</li>
						
						<li>Your personality traits associated with the unique placement of the Sun, Moon and Planets in your chart</li>
						
					</ul>	
					
				</div>
				
				<div class="col-md-6">
					
					<ul class="bullets">
						
						<li>Who you really are as a lover and how to attract YOUR kind of love</li>
						
						<li>What makes you tick, what makes you happy, and what makes you explode (and WHY)</li>
						
						<li>Insight that will lead you to living the best possible version of your life</li>
						
					</ul>
					
				</div>		
				
			</div>
			
		</div>
		
		<div class="container">		
			
			<div class="row">
				
				<div class="col-md-12">
					
					<p id="what_you_get">WHAT YOU GET</p>
					
					<div align="center"><img src="/assets/images/chart-wheel-form/mb_phone.png" id="what_you_get_phone"></div>
					
					<div align="center"><img src="/assets/images/chart-wheel-form/dt_laptop.png" id="what_you_get_laptop"></div>
					
					<div align="center"><div class="form_wrapper" style="margin-top: 30px; margin-bottom: 50px;">
						
						<div class="form">
							
							<form id="natal-chart-form_1" autocomplete="off" action="/handler.php?action=getNatalChart&autosubmit=true" method="post">

                            <input type="hidden" name="is_gdpr" value="false" />
						
                            <input type="text" name="current_first_name" id="current_first_name" class="form_input half left" placeholder="Your First Name"
                            value="<?php echo ($cookies) ? $cookies->current_first_name : $_POST['current_first_name']; ?>">
							
                            <input type="text" name="current_last_name" id="current_last_name" class="form_input half right" placeholder="Your Last Name"
                            value="<?php echo ($cookies) ? $cookies->current_last_name : $_POST['current_last_name']; ?>">
							
							<select name="birth_date_month" class="form_input form_select" id="birth_date_month">
								
                            <option label="Month" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Month</option>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_date_month == $i) || $_POST['birth_date_month'] == "$i") ? 'selected' : ''; ?>><?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?></option>
                            <?php endfor; ?>
								
							</select>
							
							<select name="birth_date_day" class="form_input form_select" id="birth_date_day">
								
                                <option label="Day" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Day</option>
                            <?php for ($r = 1; $r <= 31; $r++) { ?>
                                <option label="<?php echo $r; ?>"
                                    value="<?php echo $r; ?>" <?php echo (($cookies && $cookies->birth_date_day == $r) || $_POST['birth_date_day'] == "$r") ? 'selected' : ''; ?>><?php echo $r; ?></option>
                            <?php } ?>
							</select>
							
							<select name="birth_date_year" class="form_input form_select" id="birth_date_year">
								
                            <option label="Year" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Year</option>
                            <?php
                            // Setup default year as 1970
                            if (!$cookies || !$cookies->birth_date_year > (date('Y') - 100)) {
                                $defaultDate = 1970;
                            } else {
                                $defaultDate = (int) $cookies->birth_date_year;
                            }
                            
                            for ($i = date('Y') - 100; $i <= date('Y') - 5; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        
                                        value="<?php echo $i; ?>" <?php echo ($defaultDate === $i|| $_POST['birth_date_year'] == "$i") ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
							
							</select>
							
							<div id="time_birth_1">
							
								<select name="birth_time_hour" class="form_input form_select" id="birth_time_hour">
									
                                <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_time_hour == $i)|| $_POST['birth_time_hour'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 12) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
									
								</select>
								
								<select name="birth_time_minute" class="form_input form_select" id="birth_time_minute">
									
                                <?php for ($i = 0; $i <= 59; $i++): ?>
                                <option label="<?php echo sprintf("%02d", $i); ?>"
                                        value="<?php echo sprintf("%02d", $i); ?>" <?php echo (($cookies && $cookies->birth_time_minute == $i)|| $_POST['birth_time_minute'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 0) ? 'selected' : ''; ?>><?php echo sprintf("%02d", $i); ?></option>
                            <?php endfor; ?>
								
								</select>
							
								<select name="birth_time_meridiem" class="form_input form_select" id="birth_time_meridiem">
									
                                <option label="a.m."
                                    value="AM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'AM')|| $_POST['birth_time_meridiem'] == "AM") ? 'selected' : ''; ?>>
                                a.m.
                            </option>
                            <option label="p.m."
                                    value="PM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'PM')|| $_POST['birth_time_meridiem'] == "PM") ? 'selected' : ''; ?> <?php echo (!$cookies) ? 'selected' : ''; ?>>
                                p.m.
                            </option>
								
								</select>
							
							</div>
							
							<div style="clear: both; height: 10px;"></div>
							
                                <input type="checkbox" name="birth_time_unknown" value="1" class="form_checkbox" id="birth_time_unknown_1"<? (isset($_POST['birth_time_unknown'])) ? ' checked' : '' ?>><span class="form_checkbox_label">I do not know my time of birth</span>
							
							<div style="clear: both; height: 1px;"></div>
							
                            <input type="text" name="start_city_search" class="form_input" placeholder="City of Birth" id="birth_city_input_1" autocomplete="off" 
                            value="<?php echo ($cookies) ? $cookies->start_city_search : $_POST['start_city_search']; ?>">
																
                            <input type="text" name="send_email_address" class="form_input" id="send_email_address" placeholder="Email" value="<?php echo ($cookies) ? isset($cookies->send_email_address) ? $cookies->send_email_address : $_POST['send_email_address'] : $_POST['send_email_address']; ?>">	
							
							

							<div style="clear: both; height: 10px;"></div>
							
							<!-- GDPR content -->
        					<div class="gdpr-email-consent">
								
								<p class="form_disclaimer">Please confirm you’d like to receive email communications from Astrology.tv. You can update your email preferences at anytime. <a href="https://astrology.tv/privacy-policy/" target="_blank">Privacy&nbsp;Policy</a></p>
								
								<div style="clear: both; height: 10px;"></div>

								<input type="checkbox" id="consent-checkbox-1" name="consent-checkbox-1" value="1" class="form_checkbox"><span class="form_checkbox_label">Yes! I want to receive emails, forecasts and personalized readings.</span>

							</div>
								
							<div style="clear: both; height: 1px;"></div>
							
							<div align="center"><input type="image" class="mb_form_submit" src="/assets/images/chart-wheel-form/mb_top_form_button.png"></div>
							
							<div align="center"><input type="image" class="dt_form_submit" src="/assets/images/chart-wheel-form/dt_top_form_button.png"></div>

							<div class="form_disclaimer"><em>We use your information to deliver a personalized, accurate astrology reading. We will only use your email address to occasionally send you newsletters or announcements about cool stuff we think you’ll like, but we make it easy for you to unsubscribe from any of our mailings at any time. We will never spam you or sell your information.</em> <span class="privacy_link"><a href="https://astrology.tv/privacy-policy/" target="_blank">Privacy&nbsp;Policy</a></span></div>
							
							<input type="hidden" id="start_acs_location_id_1"
                            value="<?php echo ($cookies) ? $cookies->start_acs_location_id : $_POST['start_acs_location_id']; ?>"
                               name="start_acs_location_id">
							<input type="hidden" name="autosubmit" value="true">
						
						</form>
                            
                        </div>    
                            
						
					</div></div>				
					
				</div>
				
			</div>	
		
		</div>
			
	</div>	
	
	<div class="container-fluid" id="mb_signs_container">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<p id="signs_headline">Want Your Daily Horoscope<br />
Sent to Your Inbox Each Day?<br />
<span style="font-weight: 700;">Select Your Sign:</span></p>
					
				</div>	
				
			</div>	
			
			<div class="row">
				
				<div class="col-md-12" style="padding-left: 0; padding-right: 0;">
					
					<img src="/assets/images/chart-wheel-form/signs/aries.png" class="sign_image">
						
					<img src="/assets/images/chart-wheel-form/signs/taurus.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/gemini.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/cancer.png" class="sign_image">
					
				</div>	
				
			</div>	
			
			<div class="row">
				
				<div class="col-md-12" style="padding-left: 0; padding-right: 0;">
					
					<img src="/assets/images/chart-wheel-form/signs/leo.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/virgo.png" class="sign_image">
						
					<img src="/assets/images/chart-wheel-form/signs/libra.png" class="sign_image">
						
					<img src="/assets/images/chart-wheel-form/signs/scorpio.png" class="sign_image">
					
				</div>	
				
			</div>	
			
			<div class="row">
				
				<div class="col-md-12" style="padding-left: 0; padding-right: 0;">
					
					<img src="/assets/images/chart-wheel-form/signs/sagittarius.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/capricorn.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/aquarius.png" class="sign_image">
					
					<img src="/assets/images/chart-wheel-form/signs/pisces.png" class="sign_image">
					
				</div>	
				
			</div>	
			
		</div>	
		
	</div>
	
	<div class="container-fluid" id="mb_social_bottom">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<div align="center"><img src="/assets/images/chart-wheel-form/black_logo.png"></div>
					
				</div>	
				
			</div>
			
			<div class="row" id="social_icons_row">
				
				<div class="col-md-12">
					
					<div align="center">
					
						<img src="/assets/images/chart-wheel-form/facebook.png">
						
						<img src="/assets/images/chart-wheel-form/twitter.png">
						
						<img src="/assets/images/chart-wheel-form/youtube.png">
						
						<img src="/assets/images/chart-wheel-form/instagram.png">
						
					</div>
					
				</div>	
				
			</div>				
			
		</div>	
		
	</div>
	
	<div class="container-fluid" id="dt_signs_container">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<p id="dt_signs_headline">Want Your Daily Horoscope Sent to Your Inbox Each Day? <span style="font-weight: 700;">Select Your Sign:</span></p>
					
				</div>	
				
			</div>	
			
			<div class="row">
				
				<div class="col-md-12">
					
					<div align="center">
					
						<img src="/assets/images/chart-wheel-form/signs/aries.png" class="dt_sign_image">
							
						<img src="/assets/images/chart-wheel-form/signs/taurus.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/gemini.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/cancer.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/leo.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/virgo.png" class="dt_sign_image">
							
						<img src="/assets/images/chart-wheel-form/signs/libra.png" class="dt_sign_image">
							
						<img src="/assets/images/chart-wheel-form/signs/scorpio.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/sagittarius.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/capricorn.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/aquarius.png" class="dt_sign_image">
						
						<img src="/assets/images/chart-wheel-form/signs/pisces.png" class="dt_sign_image">
					
					</div>
					
				</div>						
				
			</div>	
			
			<div class="row">
					
				<div class="col-md-6" id="dt_bottom_logo">
						
					<img src="/assets/images/chart-wheel-form/black_logo.png">	
						
				</div>
					
				<div class="col-md-6" id="dt_bottom_social">
						
					<img src="/assets/images/chart-wheel-form/facebook.png">
						
					<img src="/assets/images/chart-wheel-form/twitter.png">
						
					<img src="/assets/images/chart-wheel-form/youtube.png">
						
					<img src="/assets/images/chart-wheel-form/instagram.png">							
						
				</div>		
					
			</div>
			
		</div>	
		
	</div>	
	
	<div class="container-fluid" id="mb_footer_container" style="display: none;">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-12">
					
					<p id="footer">&copy; ASTROLOGY.TV <a href="#">Contact Us</a><br />
<a href="#">Privacy Policy</a>  |  <a href="#">Refund Policy</a> <br />
 &copy;2019 ASTROLOGY.TV All Rights Reserved
					
				</div>	
				
			</div>		
			
		</div>	
		
	</div>
	
	<div class="container-fluid" id="dt_footer_container" style="display: none;">
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-6">
					
					<p class="dt_footer" style="text-align: left;">&copy; ASTROLOGYY.TV <a href="#">Contact Us</a> | 
<a href="#">Privacy Policy</a>  |  <a href="#">Refund Policy</a> </p>

					
				</div>	
				
				<div class="col-md-6">
					
					 <p class="dt_footer" style="text-align: right;">&copy;2019 ASTROLOGY.TV All Rights Reserved</p>
					
				</div>	
				
			</div>		
			
		</div>	
		
	</div>
	
	<div class="container-fluid" id="dt_footer_container_1">
		
		<div class="container">
			
			<div class="row" id="dt_footer_menu_row">
				
				<div class="col-md-12">
					
					<ul id="dt_footer_menu">
						
		              <li><a href="astrology.tv/privacy">Privacy Policy</a></li>
		              <li><a href="astrology.tv/terms">Terms of Use</a></li>
		              <li><a href="astrology.tv/about">About Us</a></li>
		              <li><a href="astrology.tv/horoscopes/signs">Horoscopes</a></li>
		              <li><a href="astrology.tv/reports">Reports</a></li>
		              <li><a href="astrology.tv/planets">Today's Planets</a></li>
		              
		            </ul>
					
				</div>	
				
			</div>	
			
			<div class="row" id="dt_footer_copyright_row">
				
				<div class="col-md-6">
					
					<p id="dt_footer_copyright">&copy;2019. All Rights Reserved. Astrology.TV</p>	
					
				</div>
				
				<div class="col-md-6">
					
					<p id="dt_footer_disclaimer">ClickBank is the retailer of products on this site. CLICKBANK&reg; is a registered trademark of Click Sales, Inc., a Delaware corporation located at 1444 S. Entertainment Ave., Suite 410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>
					
				</div>			
				
			</div>	
			
		</div>	
		
	</div>	
	
	
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/additional-methods.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.4.0/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.3.3/jquery.appear.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/jquery.easy-autocomplete.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>		
		<script src="/assets/js/location.js"></script> 
<script type="text/javascript">

	var setGDPR; // triggered from GTM tag
    var email_required = false;

	$(document).ready(function () {

        setGDPR = function(){
            $('.gdpr-email-consent').show();
            $('.privacy_link').hide();
            $("input[name='send_email_address']").attr("placeholder", "Email (optional)");
            $("input[name='is_gdpr']").val(true);
        }

        var readyCheck = setInterval(function(){
            if(typeof document.is_eu !== undefined || typeof window.is_us !== undefined) {
                email_required = !document.is_eu;
                run();
            }
        }, 500);

        var run = function(){
            var canvas;
            clearInterval(readyCheck);
            
    	    /**
    	     * City search autocomplete
    	     */

    	    $("#birth_city_input").easyAutocomplete({
    	        url: function (phrase) {
    	            return "https://chart.astrology.tv/handler.php"
    	        },
    	        getValue: function (element) {
    	            return element.info;
    	        },
    	        minCharNumber: 2,
    	        ajaxSettings: {dataType: "json", method: "POST", data: {dataType: "json"}},
    	        preparePostData: function (data) {
    	            data.phrase = $("#birth_city_input").val();
    	            data.action = 'getLocation';
    	            return data;
    	        },
    	        list: {
    	            match: true,
    	            maxNumberOfElements: 100,
    	            onChooseEvent: function () {
    	                var location = $("#birth_city_input").getSelectedItemData();
    	                $('#start_acs_location_id').attr('value', location.id);
    	            }
    	        },
    	        requestDelay: 400
    	    });
    	    
    	    $("#birth_city_input_1").easyAutocomplete({
    	        url: function (phrase) {
    	            return "https://chart.astrology.tv/handler.php"
    	        },
    	        getValue: function (element) {
    	            return element.info;
    	        },
    	        minCharNumber: 2,
    	        ajaxSettings: {dataType: "json", method: "POST", data: {dataType: "json"}},
    	        preparePostData: function (data) {
    	            data.phrase = $("#birth_city_input_1").val();
    	            data.action = 'getLocation';
    	            return data;
    	        },
    	        list: {
    	            match: true,
    	            maxNumberOfElements: 100,
    	            onChooseEvent: function () {
    	                var location = $("#birth_city_input_1").getSelectedItemData();
    	                $('#start_acs_location_id_1').attr('value', location.id);
    	            }
    	        },
    	        requestDelay: 400
    	    });
    	    
    	    $('#birth_time_unknown').on('change', function () {
    	        if ($('#birth_time_unknown').prop('checked')) {
    	            document.getElementById('time_birth').style.display = 'none';
    	            $('#time_birth').hide();
    	        } else {
    	            document.getElementById('time_birth').style.display = 'block';
    	            $('#time_birth').show();
    	        }
    	    });
    	    
    	    $('#birth_time_unknown_1').on('change', function () {
    	        if ($('#birth_time_unknown_1').prop('checked')) {
    	            document.getElementById('time_birth_1').style.display = 'none';
    	            $('#time_birth_1').hide();
    	        } else {
    	            document.getElementById('time_birth_1').style.display = 'block';
    	            $('#time_birth_1').show();
    	        }
    	    });
    	    
    	     /**
    	     * Validation
    	     */

    	    $.validator.setDefaults({
    	        ignore: "",
    	        errorPlacement: function (error, element) {
    	            var name = element.attr("name");

    	            if (!$('#' + name + '-error').is(':visible'))
    	                var n = noty({
    	                    layout: 'bottomCenter',
    	                    timeout: 3000,
    	                    progressBar: true,
    	                    theme: 'metroui',
    	                    text: error,
    	                    maxVisible: 1,
    	                    force: true,
    	                    type: 'error',
    	                    animation: {
    	                        open: 'animated flipInX', // Animate.css class names
    	                        close: 'animated zoomOut', // Animate.css class names
    	                        easing: 'swing', // unavailable - no need
    	                        speed: 500 // unavailable - no need
    	                    }
    	                });
    	        }
    	    });

    	    $.validator.addMethod('username', function (value) {
    	        return /^([a-zA-Z -']+)$/.test(value);
    	    }, 'Only letters, space "`" and "- " are allowed.');

    	    $.validator.addMethod("nowhitespace", function (value, element) {
    	        return $.trim(value) && value != "";
    	    }, "Can't be empty string");

    	    $.validator.addMethod("email_1", function (value, element) {
    	        return this.optional(element) || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
    	    }, 'Please enter valid email address');

    	    $("#natal-chart-form").validate({
    	        rules: {
    	            'current_first_name': {required: true, nowhitespace: true, username: true},
    	            'current_last_name': {required: true, nowhitespace: true, username: true},
    	            'birth_date_month': {required: true},
    	            'birth_date_day': {required: true},
    	            'birth_date_year': {required: true},
    	            'start_city_search': {required: true, nowhitespace: true},
					// 'start_acs_location_id': {required: true},
					'country':{required: true},
					'state':{required: true},
					'city':{required: true},
    	            'send_email_address': {required: email_required, email_1: true}
    	        },
    	        messages: {
    	            'current_first_name': {
    	                'required': 'Your First Name is required',
    	                'nowhitespace': "First Name can't be empty string"
    	            },
    	            'current_last_name': {
    	                'required': 'Your Last Name is required',
    	                'nowhitespace': "Last Name can't be empty string"
    	            },
    	            'birth_date_month': {'required': 'Your Birth Date Month is required'},
    	            'birth_date_day': {'required': 'Your Birth Date Day is required'},
    	            'birth_date_year': {'required': 'Your Birth Date Year is required'},
    	            'start_city_search': {
    	                'required': 'Your City of Birth is required',
    	                'nowhitespace': "City of Birth can't be empty string"
    	            },
					// 'start_acs_location_id': {'required': 'Please select "City of birth" from the options that appear as you type'},
					'country':{'required': 'Please select country'},
					'state':{'required': 'Please select state'},
					'city':{'required': 'Please select city'},
    	            'send_email_address': {'required': 'E-mail is required', 'nowhitespace': "E-mail can't be empty string"},
    	        }
    	    });
    	    
    	    $("#natal-chart-form_1").validate({
    	        rules: {
    	            'current_first_name': {required: true, nowhitespace: true, username: true},
    	            'current_last_name': {required: true, nowhitespace: true, username: true},
    	            'birth_date_month': {required: true},
    	            'birth_date_day': {required: true},
    	            'birth_date_year': {required: true},
    	            'start_city_search': {required: true, nowhitespace: true},
					// 'start_acs_location_id': {required: true},
					'country':{required: true},
					'state':{required: true},
					'city':{required: true},
    	            'send_email_address': {required: email_required, email_1: true}
    	        },
    	        messages: {
    	            'current_first_name': {
    	                'required': 'Your First Name is required',
    	                'nowhitespace': "First Name can't be empty string"
    	            },
    	            'current_last_name': {
    	                'required': 'Your Last Name is required',
    	                'nowhitespace': "Last Name can't be empty string"
    	            },
    	            'birth_date_month': {'required': 'Your Birth Date Month is required'},
    	            'birth_date_day': {'required': 'Your Birth Date Day is required'},
    	            'birth_date_year': {'required': 'Your Birth Date Year is required'},
    	            'start_city_search': {
    	                'required': 'Your City of Birth is required',
    	                'nowhitespace': "City of Birth can't be empty string"
    	            },
					// 'start_acs_location_id': {'required': 'Please select "City of birth" from the options that appear as you type'},
					'country':{'required': 'Please select country'},
					'state':{'required': 'Please select state'},
					'city':{'required': 'Please select city'},
    	            'send_email_address': {'required': 'E-mail is required', 'nowhitespace': "E-mail can't be empty string"},
    	        }
    	    });


    	    $('#submit-natal-chart-form').on('click', function () {
    	        if ($('#birth_time_unknown').prop('checked')) {
    	            $('input[name="start_city_search"]').rules("add", {
    	                required: false,
    	                nowhitespace: false
    	            });
    	            // $('input[name="start_acs_location_id"]').rules("add", {
    	            //     required: false
					// });
					$('input[name="country"]').rules("add", {
    	                required: false
					});
					$('input[name="state"]').rules("add", {
    	                required: false
					});
					$('input[name="city"]').rules("add", {
    	                required: false
    	            });
    	        } else {
    	            $('input[name="start_city_search"]').rules("add", {
    	                required: true,
    	                nowhitespace: false,
    	                messages: {
    	                    required: "Your City of Birth is required",
    	                    nowhitespace: "City of Birth can't be empty string"
    	                }
    	            });
    	            // $('input[name="start_acs_location_id"]').rules("add", {
    	            //     required: true,
    	            //     messages: {
    	            //         required: 'Please select "City of birth" from the options that appear as you type'
    	            //     }
					// });
					$('input[name="country"]').rules("add", {
    	                required: true,
    	                messages: {
    	                    required: 'Please select country'
    	                }
					});
					$('input[name="state"]').rules("add", {
    	                required: true,
    	                messages: {
    	                    required: 'Please select state'
    	                }
					});
					$('input[name="city"]').rules("add", {
    	                required: true,
    	                messages: {
    	                    required: 'Please select city'
    	                }
    	            });
    	        }

    	        if ($("#natal-chart-form").valid()) {
    	            var dob = $('#birth_date_month').val() + '/' + $('#birth_date_day').val() + '/' + $('#birth_date_year').val();
    	            //validate dob for errors: ex. 31 of Februaby - is error, etc
    	            if (isDate(dob)) {

    	                $('.fullname-title').text($('#current_first_name').val() + ' ' + $('#current_last_name').val())
    	                $('.dob-title').text(dob);
    	                $('.city-title').text($('#start_city_search').val());

    	                //set user astrology natal chart(anc) detail data cookies
    	                $.jCookies({
    	                    name: 'anc-details',
    	                    value: {
    	                        current_first_name: $('#current_first_name').val(),
    	                        current_last_name: $('#current_last_name').val(),
    	                        birth_date_month: $('#birth_date_month').val(),
    	                        birth_date_day: $('#birth_date_day').val(),
    	                        birth_date_year: $('#birth_date_year').val(),
    	                        birth_time_hour: $('#birth_time_hour').val(),
    	                        birth_time_minute: $('#birth_time_minute').val(),
    	                        birth_time_meridiem: $('#birth_time_meridiem').val(),
    	                        start_city_search: $('#start_city_search').val(),
								// start_acs_location_id: $('#start_acs_location_id').val(),
								country: $('#countryId').val(),
								state: $('#stateId').val(),
								city: $('#cityId').val(),
    	                        send_email_address: $('#send_email_address').val(),
    	                    },
    	                    days: 365
    	                });

    	                getNatalChart();

    	            } else {

    	                //clear month and day values and validate form to show errors
    	                $('#birth_date_month').val('');
    	                $('#birth_date_day').val('');

    	                //validate form for error appears
    	                $('#user-details-form').valid();
    	            }
    	        }
    	    });
        };
	});
</script>
	
	
	
	</body>
</html>