<div class="container">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-main">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbar-main">
                <ul class="nav navbar-nav">
                    <li><a target="_blank" id="facebook-share-link" href="#"><i
                                    class="fa fa-lg fa-facebook"></i></a></li>
                    <li><a target="_blank" id="twitter-share-link" href="#"><i class="fa fa-lg fa-twitter"></i></a>
                    </li>
                    <!-- <li><a id="send-email-open" style="cursor: pointer"><i class="fa fa-lg fa-envelope"></i></a>
                    </li>
                    <li><a id="send-email-popup-open" href="#send-email-popup" class="send-to-a-friend-link">Send
                            to a friend <i class="fa fa-xs fa-envelope-o"></i></a></li>
                    <li>
                        <form action="/handler.php" target="_blank" method="post">
                            <button target="_blank" href="#" class="save-in-pdf-link" disabled=disabled>Save <i class="fa fa-print"></i></button>
                            <input type="hidden" name="pageContent" value="" id="pageContent" />
                            <input type="hidden" name="imageContent" value="" id="imageContent" />
                            <input type="hidden" name="action" value="saveChartInPdf" />
                        </form>
                    </li> -->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" id="goToEditForm"><i class="fa fa-lg fa-pencil"></i>Edit birth data info</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
