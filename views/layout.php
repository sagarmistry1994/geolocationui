<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Personalized Natal Chart</title>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"/>

    <link rel="stylesheet" href="assets/fonts/montserrat/montserrat.css"/>
    <link rel="stylesheet" href="assets/fonts/oswald/oswald.css"/>
    <link rel="stylesheet" href="assets/fonts/astro/astro.css"/>
    <link rel="stylesheet" href="assets/css/styles.css"/>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MHBL97K');</script>
    <!-- End Google Tag Manager -->
    <? $display_chart = "display-none"; ?>
    <? $display_form = ""; ?>
    <? if (isset($chart_info)): ?>
    <script type="text/javascript"> 
    var chartInfo = <?=$chart_info;?>
    </script>
    <? $display_chart = ""; ?>
    <? $display_form = "display-none"; ?>
    <? endif; ?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MHBL97K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php require_once("send-email-popup.php") ?>

<?php require_once("main-form.php") ?>


<div id="report-container" class="container-fluid <?=$display_chart?>">
    <?php require_once("main-menu.php") ?>
    <div class="row">
        <div class="col-xs-12 col-md-7 text-center" id="natal-chart-column">
            <div id="natal-chart-image">
                <div class="canvas-natal-cart">
                    <button class="settings-button">SETTINGS</button>
                    <div class="settings-popup">
                        <span class="settings-title">SETTINGS</span>
                        <span class="settings-close">X</span>
                        <div class="checkbox">
                            <input id="disableBackground" type="checkbox" name="check" value="disableBackground">
                            <label for="disableBackground">TURN OFF ALL IMAGES</label>
                        </div>
                        <div class="checkbox">
                            <input id="disableAspects" type="checkbox" name="check" value="disableAspects">
                            <label for="disableAspects">TURN OFF ALL Aspect Lines</label>
                        </div>

                    </div>
                </div>
            </div>
            <div id="planet-sign"></div>
        </div>
        <div class="col-xs-12 col-md-5 text-left" id="natal-chart-report"></div>
    </div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/additional-methods.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.4.0/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.3.3/jquery.appear.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/jquery.easy-autocomplete.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="assets/js/jcookies.js"></script>
<script type="text/javascript" src="assets/js/fontfaceobserver.js"></script>

<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>