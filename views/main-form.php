<div id="form-container" class="container <?=$display_form;?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-header text-center">
                <h1>Get Your Personalized Natal Chart Now, 100% Free:</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <form id="natal-chart-form" class="form-horizontal">

                <div class="form-group">
                    <label for="current_first_name" class="col-sm-3 control-label">Your First Name:</label>
                    <div class="col-sm-7">
                        <input type="text" id="current_first_name" name="current_first_name" class="form-control"
                               placeholder="Your First Name"
                               value="<?php echo ($cookies) ? $cookies->current_first_name : $_POST['current_first_name']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="current_last_name" class="col-sm-3 control-label">Your Last Name:</label>
                    <div class="col-sm-7">
                        <input type="text" id="current_last_name" name="current_last_name" class="form-control"
                               placeholder="Your Last Name"
                               value="<?php echo ($cookies) ? $cookies->current_last_name : $_POST['current_last_name']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Date of birth:</label>
                    <div class="col-sm-2">
                        <select id="birth_date_month" name="birth_date_month" class="form-control">
                            <option label="Month" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Month</option>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_date_month == $i) || $_POST['birth_date_month'] == "$i") ? 'selected' : ''; ?>><?php echo DateTime::createFromFormat('!m', sprintf("%02d", $i))->format('F'); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select id="birth_date_day" name="birth_date_day" class="form-control">
                            <option label="Day" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Day</option>
                            <?php for ($i = 1; $i <= 31; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_date_day == $i) || $_POST['birth_date_day'] == "$i") ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select id="birth_date_year" name="birth_date_year" class="form-control">
                            <option label="Year" value="" <?php echo (!$cookies) ? 'selected' : ''; ?>>Year</option>
                            <?php
                            // Setup default year as 1970
                            if (!$cookies || !$cookies->birth_date_year > (date('Y') - 100)) {
                                $defaultDate = 1970;
                            } else {
                                $defaultDate = (int) $cookies->birth_date_year;
                            }
                            
                            for ($i = date('Y') - 100; $i <= date('Y') - 5; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo ($defaultDate === $i|| $_POST['birth_date_year'] == "$i") ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="time_birth">
                    <label for="" class="col-sm-3 control-label">Time of birth:</label>
                    <div class="col-sm-2">
                        <select id="birth_time_hour" name="birth_time_hour" class="form-control">
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option label="<?php echo $i; ?>"
                                        value="<?php echo $i; ?>" <?php echo (($cookies && $cookies->birth_time_hour == $i)|| $_POST['birth_time_hour'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 12) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select id="birth_time_minute" name="birth_time_minute" class="form-control">
                            <?php for ($i = 0; $i <= 59; $i++): ?>
                                <option label="<?php echo sprintf("%02d", $i); ?>"
                                        value="<?php echo sprintf("%02d", $i); ?>" <?php echo (($cookies && $cookies->birth_time_minute == $i)|| $_POST['birth_time_minute'] == "$i") ? 'selected' : ''; ?> <?php echo (!$cookies && $i == 0) ? 'selected' : ''; ?>><?php echo sprintf("%02d", $i); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select id="birth_time_meridiem" name="birth_time_meridiem" class="form-control">
                            <option label="a.m."
                                    value="AM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'AM')|| $_POST['birth_time_meridiem'] == "AM") ? 'selected' : ''; ?>>
                                a.m.
                            </option>
                            <option label="p.m."
                                    value="PM" <?php echo (($cookies && $cookies->birth_time_meridiem == 'PM')|| $_POST['birth_time_meridiem'] == "PM") ? 'selected' : ''; ?> <?php echo (!$cookies) ? 'selected' : ''; ?>>
                                p.m.
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" id="birth_time_unknown" name="birth_time_unknown" value="1"<? (isset($_POST['birth_time_unknown'])) ? ' checked' : '' ?>>
                            <label for="birth_time_unknown">I do not know my time of birth</label>
                            <p style="max-width: 480px;">
                                <small class="text-info">* For an unknown birth time we use the natural house system.
                                    This allocates the planets to their related zodiacal houses. The houses are of equal
                                    size and based on 0 degrees Aries ascendant or rising sign and each house cusp
                                    thereafter in zodiacal order.
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="city_birth">
                <label for="current_last_name" class="col-sm-3 control-label">City of birth:</label>
                <!-- Geolocation -->
                <select name="country" class="form_input form_select countries" id="countryId">
                <option value="">Select Country</option>
                </select>
                <select name="state" class="form_input form_select states" id="stateId" required="required">
                <option value="">Select State</option>
                </select>
                <select name="city" class="form_input form_select cities" id="cityId" required="required">
                <option value="">Select City</option>
                </select>
                <!-- Geolocation -->
                </div>
                <!-- <div class="form-group" id="city_birth">
                    <label for="current_last_name" class="col-sm-3 control-label">City of birth:</label>
                    <div class="col-sm-7">
                        <input type="text" id="start_city_search" name="start_city_search" class="form-control"
                               value="<?php echo ($cookies) ? $cookies->start_city_search : $_POST['start_city_search']; ?>"
                               placeholder="City of birth">
                        <p>
                            <small class="text-info">* Select from the options that appear as you type</small>
                        </p>
                        <input type="hidden" id="start_acs_location_id"
                               value="<?php echo ($cookies) ? $cookies->start_acs_location_id : $_POST['start_acs_location_id']; ?>"
                               name="start_acs_location_id">
                    </div>
                </div> -->

                <div class="form-group">
                    <label for="send_email_address" class="col-sm-3 control-label">E-mail:</label>
                    <div class="col-sm-7">
                        <input type="email" id="send_email_address" name="send_email_address" class="form-control"
                               placeholder="Your E-mail"
                               value="<?php echo ($cookies) ? isset($cookies->send_email_address) ? $cookies->send_email_address : $_POST['send_email_address'] : $_POST['send_email_address']; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-10">
                        <button type="button" id="submit-natal-chart-form" class="btn submit-natal-chart-button">
                            Prepare My Personalized Chart Now
                        </button>
                        <input type="hidden" name="action" value="getNatalChart">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <span style="font-family: 'Astro'; font-size:1px">L</span>
    <span style="font-family: 'Montserrat'; font-size:1px">L</span>
    <span style="font-family: 'Oswald'; font-size:1px">L</span>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/add-on/jplayer.playlist.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/additional-methods.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.4.0/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.3.3/jquery.appear.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.numerologist.com/free-report/js/jquery.easy-autocomplete.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>		
		<script src="/assets/js/location.js"></script> 