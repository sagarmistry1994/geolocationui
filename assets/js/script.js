var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


$(document).keydown(function (event) {
    if ((event.metaKey == true || event.ctrlKey == true) && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109' || event.which == '187' || event.which == '189')) {
        event.preventDefault();
    }
    // 107 Num Key  +
    // 109 Num Key  -
    // 173 Min Key  hyphen/underscor Hey
    // 61 Plus key  +/= key
    // 91 comand
});

$(window).bind('mousewheel DOMMouseScroll', function (event) {
    if (event.ctrlKey == true || event.metaKey == true) {
        event.preventDefault();
    }
});

$(document).ready(function () {
    var canvas;
    /**
     * City search autocomplete
     */

    $("#start_city_search").easyAutocomplete({
        url: function (phrase) {
            return "handler.php"
        },
        getValue: function (element) {
            return element.info;
        },
        minCharNumber: 2,
        ajaxSettings: {dataType: "json", method: "POST", data: {dataType: "json"}},
        preparePostData: function (data) {
            data.phrase = $("#start_city_search").val();
            data.action = 'getLocation';
            return data;
        },
        list: {
            match: true,
            maxNumberOfElements: 100,
            onChooseEvent: function () {
                var location = $("#start_city_search").getSelectedItemData();
                $('#start_acs_location_id').attr('value', location.id);
            }
        },
        requestDelay: 400
    });

    /**
     * Birthtime ckeckbox
     */

    $('#birth_time_unknown').on('change', function () {
        if ($('#birth_time_unknown').prop('checked')) {
            document.getElementById('time_birth').style.display = 'none';
            $('#time_birth').hide();
        } else {
            document.getElementById('time_birth').style.display = 'block';
            $('#time_birth').show();
        }
    });

    $('#goToEditForm').on('click', function () {
        $('#report-container').fadeOut(300, function () {
            window.chartInfo = undefined;
            window.location.href="/";
        });
    });

    $('.save-in-pdf-link').on('click', function () {
        // event.preventDefault();
        saveImg();
        // setTimeout(function() {
        //     window.open($('.save-in-pdf-link').attr('href'),'_blank');
        // }, 1000);

    });


    /**
     * Validation
     */

    $.validator.setDefaults({
        ignore: "",
        errorPlacement: function (error, element) {
            var name = element.attr("name");

            if (!$('#' + name + '-error').is(':visible'))
                var n = noty({
                    layout: 'bottomCenter',
                    timeout: 3000,
                    progressBar: true,
                    theme: 'metroui',
                    text: error,
                    maxVisible: 1,
                    force: true,
                    type: 'error',
                    animation: {
                        open: 'animated flipInX', // Animate.css class names
                        close: 'animated zoomOut', // Animate.css class names
                        easing: 'swing', // unavailable - no need
                        speed: 500 // unavailable - no need
                    }
                });
        }
    });

    $.validator.addMethod('username', function (value) {
        return /^([a-zA-Z -']+)$/.test(value);
    }, 'Only letters, space "`" and "- " are allowed.');

    $.validator.addMethod("nowhitespace", function (value, element) {
        return $.trim(value) && value != "";
    }, "Can't be empty string");

    $.validator.addMethod("email_1", function (value, element) {
        return this.optional(element) || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
    }, 'Please enter valid email address');

    $("#natal-chart-form").validate({
        rules: {
            'current_first_name': {required: true, nowhitespace: true, username: true},
            'current_last_name': {required: true, nowhitespace: true, username: true},
            'birth_date_month': {required: true},
            'birth_date_day': {required: true},
            'birth_date_year': {required: true},
            'start_city_search': {required: true, nowhitespace: true},
            'start_acs_location_id': {required: true},
            'send_email_address': {required: false, email_1: true}
        },
        messages: {
            'current_first_name': {
                'required': 'Your First Name is required',
                'nowhitespace': "First Name can't be empty string"
            },
            'current_last_name': {
                'required': 'Your Last Name is required',
                'nowhitespace': "Last Name can't be empty string"
            },
            'birth_date_month': {'required': 'Your Birth Date Month is required'},
            'birth_date_day': {'required': 'Your Birth Date Day is required'},
            'birth_date_year': {'required': 'Your Birth Date Year is required'},
            'start_city_search': {
                'required': 'Your City of Birth is required',
                'nowhitespace': "City of Birth can't be empty string"
            },
            // 'start_acs_location_id': {'required': 'Please select "City of birth" from the options that appear as you type'},
            'country': {'required': 'Please select "City of birth" from the options that appear as you type'},
            'state': {'required': 'Please select "City of birth" from the options that appear as you type'},
            'city': {'required': 'Please select "City of birth" from the options that appear as you type'},
            'send_email_address': {'required': 'E-mail is required', 'nowhitespace': "E-mail can't be empty string"},
        }
    });


    $('#submit-natal-chart-form').on('click', function () {
        if ($('#birth_time_unknown').prop('checked')) {
            $('input[name="start_city_search"]').rules("add", {
                required: false,
                nowhitespace: false
            });
            // $('input[name="start_acs_location_id"]').rules("add", {
            //     required: false
            // });
            $('input[name="country"]').rules("add", {
                required: false
            });
            $('input[name="state"]').rules("add", {
                required: false
            });
            $('input[name="city"]').rules("add", {
                required: false
            });
        } else {
            $('input[name="start_city_search"]').rules("add", {
                required: true,
                nowhitespace: false,
                messages: {
                    required: "Your City of Birth is required",
                    nowhitespace: "City of Birth can't be empty string"
                }
            });
            // $('input[name="start_acs_location_id"]').rules("add", {
            //     required: true,
            //     messages: {
            //         required: 'Please select "City of birth" from the options that appear as you type'
            //     }
            // });
            $('input[name="country"]').rules("add", {
                required: true,
                messages: {
                    required: 'Please select country'
                }
            });
            $('input[name="state"]').rules("add", {
                required: true,
                messages: {
                    required: 'Please select state'
                }
            });
            $('input[name="city"]').rules("add", {
                required: true,
                messages: {
                    required: 'Please select city'
                }
            });
        }

        if ($("#natal-chart-form").valid()) {
            var dob = $('#birth_date_month').val() + '/' + $('#birth_date_day').val() + '/' + $('#birth_date_year').val();
            //validate dob for errors: ex. 31 of Februaby - is error, etc
            if (isDate(dob)) {

                $('.fullname-title').text($('#current_first_name').val() + ' ' + $('#current_last_name').val())
                $('.dob-title').text(dob);
                $('.city-title').text($('#start_city_search').val());

                //set user astrology natal chart(anc) detail data cookies
                $.jCookies({
                    name: 'anc-details',
                    value: {
                        current_first_name: $('#current_first_name').val(),
                        current_last_name: $('#current_last_name').val(),
                        birth_date_month: $('#birth_date_month').val(),
                        birth_date_day: $('#birth_date_day').val(),
                        birth_date_year: $('#birth_date_year').val(),
                        birth_time_hour: $('#birth_time_hour').val(),
                        birth_time_minute: $('#birth_time_minute').val(),
                        birth_time_meridiem: $('#birth_time_meridiem').val(),
                        start_city_search: $('#start_city_search').val(),
                        // start_acs_location_id: $('#start_acs_location_id').val(),
                        country: $('#countryId').val(),
                        state: $('#stateId').val(),
                        city: $('#cityId').val(),
                        send_email_address: $('#send_email_address').val(),
                    },
                    days: 365
                });

                getNatalChart();

            } else {

                //clear month and day values and validate form to show errors
                $('#birth_date_month').val('');
                $('#birth_date_day').val('');

                //validate form for error appears
                $('#user-details-form').valid();
            }
        }
    });

    function getAstroCharacter(character) {
        var map = {
            "aries": "A",
            "taurus": "B",
            "gemini": "C",
            "cancer": "D",
            "leo": "E",
            "virgo": "F",
            "libra": "G",
            "scorpio": "H",
            "scorpius": "H",
            "sagittarius": "I",
            "capricornus": "J",
            "capricorn": "J",
            "aquarius": "K",
            "pisces": "L",
            "sun": "Q",
            "moon": "R",
            "mercury": "S",
            "venus": "T",
            "mars": "U",
            "jupiter": "V",
            "saturn": "W",
            "uranus": "X",
            "neptune": "Y",
            "pluto": "Z",
            "chiron": "T",
            "north node": "<",
            "south node": ">"
        }

        return map[character];
    }


    /**
     * FUNCTIONS
     * */

    function getNatalChart() {
        var runChart = function (natalChart) {
            var report = '';
            // should update this now that email is optional...
            $.each(natalChart.planetsContent, function (planet, content) {
                report += '<div class="bg-planets-content">';
                report += '<h3 class="text-center" id="' + planet.toUpperCase() 
                        + '"><i class="glyph">'+getAstroCharacter(planet)
                        +'</i> ' + content.heading + '</h3>';
                report += '<p>' + content.content + '</p><p>'+
                '<a href="https://try.astrology.tv/atv/personality-profile/?email='+natalChart.userInfo.email+'">Get my full report...</a></p>';
                report += '</div>';
            });

            $('#natal-chart-report').html(report);
            var zodiac = natalChart.userInfo.zodiacName;

            $('#planet-sign').html(
                '<img src="/assets/images/background-glyphs/' +
                zodiac + '-sign.png' + 
                '" alt="' +
                zodiac.charAt(0).toUpperCase() +
                zodiac.slice(1) +
                ' Zodiac Sign" class="zodiac-sign" />'
            );

            setBackgroundDependOfSettings(natalChart.userInfo.background);
            //закрываем настройки
            $(".settings-popup").hide();
            //закрываем меню
            $('.navbar-collapse').collapse('hide');

            $('#report-container').fadeIn();
            //setTimeout(function() {return 1;}, 1000); // Quick fix for font icons not loading in chart
            // stop the canvas from rendering until the fonts are loaded
            var chartFont_Oswald = new FontFaceObserver('Oswald');
            var chartFont_Montserrat = new FontFaceObserver('Montserrat');
            var chartFont_Astro = new FontFaceObserver('Astro');

            Promise.all([chartFont_Astro.load(), chartFont_Oswald.load(), chartFont_Montserrat.load()]).then(function () {
              drawNatalChartCanvas(natalChart);
            });

            $("#disableBackground").on("change", function () {
                setBackgroundDependOfSettings(natalChart.userInfo.background);
            });
        }

        if (window.chartInfo) {
            var natalChart = window.chartInfo;
            runChart(natalChart)
        } else {
            var data = $('#natal-chart-form').serialize();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'handler.php',
                data: data,
                success: function (callback) {
                    $('#form-container').fadeOut(300, function () {
                        $(window).scrollTop(0);
    
                        // данные карты с сервера
                        var natalChart = $.parseJSON(callback.outputData);
                        runChart(natalChart)
    
                    });
                }
            });
        }

        
    }

    function drawNatalChartCanvas(natalChart) {
        // добавление канваса
        $('.canvas-natal-cart').prepend('<canvas id="c" width="' + natalChart.chartWidth + '" height="' + natalChart.chartHeight + '" style="max-width: 100%; max-height: 100%;"></canvas>');

        canvas = this.__canvas = new fabric.Canvas('c', {
            targetFindTolerance: 2,
            hoverCursor: 'default',
            selection: false,
            allowTouchScrolling: true,
            enableRetinaScaling: false
        });

        $('.canvas-container').css({position: 'absolute', top: 0, bottom: 0, left: 0, right: 0});

        /**
         * Text in corner
         */
        canvas.add(new fabric.Text(natalChart.userInfo.title.text, {
            left: 20,
            top: 20,
            fill: 'rgb(0, 0, 0)',
            fontSize: natalChart.userInfo.title.fontSize,
            fontWeight: 900,
            angle: 0,
            fontFamily: 'Montserrat',
        }));

        canvas.add(new fabric.Text(natalChart.userInfo.name.text, {
            left: 20,
            top: 70,
            fill: 'rgb(0, 0, 0)',
            fontSize: natalChart.userInfo.name.fontSize,
            angle: 0,
            fontFamily: 'Oswald',
        }));

        canvas.add(new fabric.Text(natalChart.userInfo.date.text, {
            left: 20,
            top: 115,
            fill: 'rgb(0, 0, 0)',
            fontSize: natalChart.userInfo.date.fontSize,
            angle: 0,
            fontFamily: 'Oswald',
        }));

        canvas.add(new fabric.Text(natalChart.userInfo.address.text, {
            left: 20,
            top: 160,
            fill: 'rgb(0, 0, 0)',
            fontSize: natalChart.userInfo.address.fontSize,
            angle: 0,
            fontFamily: 'Oswald',
        }));

        /**
         * *** Text in corner
         */

        /**
         * Wheels
         */
            //внешний серый круг
        var middleSignRingRadius = natalChart.signRingOuterRadius - natalChart.signRingNotchRadius / 2;
        var signRing = new fabric.Circle({
            left: natalChart.chartWidth / 2 - natalChart.signRingOuterRadius,
            top: natalChart.chartHeight / 2 - natalChart.signRingOuterRadius,
            radius: middleSignRingRadius,
            fill: 'transparent',
            stroke: natalChart.signRingColor,
            strokeWidth: natalChart.signRingNotchRadius,
        });

        //внутренний белый круг
        var middleHouseRingRadius = natalChart.houseRingOuterRadius - natalChart.houseRingNotchRadius / 2;
        var houseRing = new fabric.Circle({
            left: natalChart.chartWidth / 2 - natalChart.houseRingOuterRadius,
            top: natalChart.chartHeight / 2 - natalChart.houseRingOuterRadius,
            radius: middleHouseRingRadius,
            fill: 'transparent',
            stroke: natalChart.houseRingColor,
            strokeWidth: natalChart.houseRingNotchRadius,
        });

        //внутренний серый круг на границе меньшего радиуса белого круга
        var houseRingStroke = new fabric.Circle({
            left: natalChart.chartWidth / 2 - natalChart.houseRingInnerRadius,
            top: natalChart.chartHeight / 2 - natalChart.houseRingInnerRadius,
            radius: natalChart.houseRingInnerRadius,
            fill: 'transparent',
            stroke: natalChart.houseInnerRingColor,
            strokeWidth: 1,
        });

        canvas.add(signRing, houseRing, houseRingStroke);

        //Рисуем крупные насечки между зодиаками
        $.each(natalChart.signOffset, function (i, e) {
            drawPartialPolarLine(natalChart, canvas, Math.round(e), natalChart.signRingInnerRadius, natalChart.signRingNotchRadius, natalChart.houseRingColor);
        });

        //Рисуем иконки знаки зодиаков
        $.each(natalChart.signOffset, function (i, e) {
            var angle = e + natalChart.signGlyph.starSignSlice;
            drawCenteredText(natalChart, canvas, angle, middleSignRingRadius, natalChart.signFont[i], natalChart.signGlyph.fontSize, natalChart.signGlyph.color, false, 'Astro');
        });

        // AC MC DC IC и насечки домов
        var ascendant = natalChart.hc1[1];

        var ascedantAbsoluteAngle;
        var midheavenAbsoluteAngle;
        var descedantAbsoluteAngle;
        var imumcoeliAbsoluteAngle;

        var houseAngles = [];
        var houseCounter = 1;
        $.each(natalChart.houses, function (house, details) {
            // Find the angle of the house
            houseAbsoluteAngle = ascendant - natalChart.hc1[houseCounter] - 90;
            if (houseAbsoluteAngle < 0) houseAbsoluteAngle = houseAbsoluteAngle + 360;

            //Рисуем насечки домов
            drawPartialPolarLine(natalChart, canvas, Math.round(houseAbsoluteAngle), natalChart.houseRingInnerRadius, natalChart.houseRingNotchRadius, natalChart.signRingColor);

            if (house == 'Ascendant') {
                ascedantAbsoluteAngle = houseAbsoluteAngle;
                descedantAbsoluteAngle = houseAbsoluteAngle - 180;
                if (descedantAbsoluteAngle <= 0) descedantAbsoluteAngle += 360;

                var ascedantGliph = natalChart.signFont.ascendant;
                var descedantGliph = natalChart.signFont.descendant;
                drawCenteredText(natalChart, canvas, ascedantAbsoluteAngle, natalChart.acMcGlyph.distance_1, ascedantGliph, natalChart.acMcGlyph.fontSize, natalChart.acMcGlyph.color, 0, 'Arial');
                drawCenteredText(natalChart, canvas, descedantAbsoluteAngle, natalChart.acMcGlyph.distance_1, descedantGliph, natalChart.acMcGlyph.fontSize, natalChart.acMcGlyph.color, 0, 'Arial');

                //цифры рядом с AC DC
                var ascedantNumGliph = details.deg + "'" + details.min;
                ascedantNumAbsoluteAngle = ascedantAbsoluteAngle - natalChart.acMcGlyph.textOffset_1;
                if (ascedantNumAbsoluteAngle <= 0) ascedantNumAbsoluteAngle += 360;

                descedantNumAbsoluteAngle = descedantAbsoluteAngle + natalChart.acMcGlyph.textOffset_1;
                if (descedantNumAbsoluteAngle > 360) descedantNumAbsoluteAngle -= 360
                drawCenteredText(natalChart, canvas, ascedantNumAbsoluteAngle, natalChart.acMcGlyph.distance_1, ascedantNumGliph, natalChart.acMcGlyph.fontSizeNum, natalChart.acMcGlyph.color, 0, 'Arial');
                drawCenteredText(natalChart, canvas, descedantNumAbsoluteAngle, natalChart.acMcGlyph.distance_1, ascedantNumGliph, natalChart.acMcGlyph.fontSizeNum, natalChart.acMcGlyph.color, 0, 'Arial');
            }
            if (house == 'MC (Midheaven)') {
                midheavenAbsoluteAngle = houseAbsoluteAngle;
                imumcoeliAbsoluteAngle = houseAbsoluteAngle - 180;
                if (imumcoeliAbsoluteAngle <= 0) imumcoeliAbsoluteAngle += 360;

                var midheavenGliph = natalChart.signFont['mc (midheaven)'];
                var imumcoeli = natalChart.signFont.ic;
                drawCenteredText(natalChart, canvas, midheavenAbsoluteAngle, natalChart.acMcGlyph.distance, midheavenGliph, natalChart.acMcGlyph.fontSize, natalChart.acMcGlyph.color, 0, 'Arial');
                drawCenteredText(natalChart, canvas, imumcoeliAbsoluteAngle, natalChart.acMcGlyph.distance, imumcoeli, natalChart.acMcGlyph.fontSize, natalChart.acMcGlyph.color, 0, 'Arial');

                //цифры рядом с MC IC
                var midheavenNumGliph = details.deg + "'" + details.min;
                midheavenNumAbsoluteAngle = midheavenAbsoluteAngle + natalChart.acMcGlyph.textOffset + 1;
                if (midheavenNumAbsoluteAngle > 360) midheavenNumAbsoluteAngle -= 360

                imumcoeliNumAbsoluteAngle = imumcoeliAbsoluteAngle - natalChart.acMcGlyph.textOffset;
                if (imumcoeliNumAbsoluteAngle > 360) imumcoeliNumAbsoluteAngle -= 360
                drawCenteredText(natalChart, canvas, midheavenNumAbsoluteAngle, natalChart.acMcGlyph.distance, midheavenNumGliph, natalChart.acMcGlyph.fontSizeNum, natalChart.acMcGlyph.color, 0, 'Arial');
                drawCenteredText(natalChart, canvas, imumcoeliNumAbsoluteAngle, natalChart.acMcGlyph.distance, midheavenNumGliph, natalChart.acMcGlyph.fontSizeNum, natalChart.acMcGlyph.color, 0, 'Arial');
            }

            houseAngles[houseCounter] = houseAbsoluteAngle;
            houseCounter++;
        });

        // рисуем номера вокруг маленького круга
        $.each(houseAngles, function (houseNumber, currentAngle) {
            var nextHouseNumber = Number(houseNumber) + 1;
            var nextAngle = (houseNumber < 12 ? houseAngles[nextHouseNumber] : houseAngles[1]);
            var diff = (currentAngle > nextAngle ? currentAngle - nextAngle : (currentAngle + 360) - nextAngle);
            var newAngle = currentAngle - (diff / 2);

            if (newAngle <= 0) {
                newAngle += 360;
            }
            drawCenteredText(natalChart, canvas, newAngle, natalChart.houseNumberGliph.distance, houseNumber.toString(), natalChart.houseNumberGliph.fontSize, natalChart.houseNumberGliph.color, 0, 'Arial');
        });
        /**
         * *** Wheels
         */

            //убираем двухсторонние дубли линий аспектов
        var uniqueAspects = [];
        var usedAspects = [];
        $.each(natalChart.aspects, function (i, aspect) {
            var used = false;
            var arr = [aspect.Planet1, aspect.Planet2];
            arr.sort();
            $.each(usedAspects, function (i, uAspect) {
                if (uAspect.length == arr.length && uAspect.every(function (v, i) {
                    return v === arr[i];
                })) {
                    used = true;
                }
            });
            if (used === false) {
                uniqueAspects.push(aspect);
            }
            usedAspects.push(arr);
        });

        //Рисуем линии аспектов
        $.each(uniqueAspects, function (i, aspect) {
            if (natalChart.ignoreAspects.indexOf(aspect.Planet1) == -1 && natalChart.ignoreAspects.indexOf(aspect.Planet2) == -1) {
                if (typeof (natalChart.planetPositions[aspect.Planet1]) !== 'undefined' && typeof (natalChart.planetPositions[aspect.Planet2]) !== 'undefined') {
                    var type = 'aspect_lines';
                    // сплошные линии
                    if (natalChart.aspectStyles[aspect.Aspect].style == 'line') {
                        //Рисуем цветные линии
                        canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                            natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                            stroke: 'rgb(' + natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                            strokeWidth: 2,
                            perPixelTargetFind: true,
                            hasControls: false,
                            hasBorders: false,
                            type: type,
                        }));
                    }
                    // пунктирные линии
                    else if (natalChart.aspectStyles[aspect.Aspect].style == 'dashed') {
                        canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                            natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                            stroke: 'rgb(' + natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                            strokeWidth: 2,
                            strokeDashArray: [20, 20],
                            perPixelTargetFind: true,
                            hasControls: false,
                            hasBorders: false,
                            type: type,
                        }));
                    }
                    // точечные линии
                    else if (natalChart.aspectStyles[aspect.Aspect].style == 'dotted') {
                        canvas.add(new fabric.Line([natalChart.planetPositions[aspect.Planet1].x, natalChart.planetPositions[aspect.Planet1].y,
                            natalChart.planetPositions[aspect.Planet2].x, natalChart.planetPositions[aspect.Planet2].y], {
                            stroke: 'rgb(' + natalChart.aspectStyles[aspect.Aspect].color.R + ',' + natalChart.aspectStyles[aspect.Aspect].color.G + ',' + natalChart.aspectStyles[aspect.Aspect].color.B + ')',
                            strokeWidth: 2,
                            strokeDashArray: [4, 5],
                            perPixelTargetFind: true,
                            hasControls: false,
                            hasBorders: false,
                            type: type,
                        }));
                    }

                }
            }
        });

        // рисуем иконки планет в маленьком круге
        var distanceChangeAmount = -50,
            previousDistance = natalChart.planetGlyph.distance,
            previousAngle =  -1, // natalChart.kSortedPlanets[0].angle,
            planetGlyphMargin = 10;
        
        $.each(natalChart.kSortedPlanets, function (i, planetDetails) {
            if (typeof (planetDetails.planet) !== 'undefined') {
                var title = planetDetails.planet + ' in ' + natalChart.planets[planetDetails.planet].sign;
                var distance = natalChart.planetGlyph.distance
                var lineMultiplyer = 1
                
                // If we're not on the first glyph
                if (previousAngle != -1) {
                    // If both glyphs are at the same radius
                    if (previousDistance == distance) {
                        // Calculate the distance between the two glyphs
                        var angleDiff = planetDetails.angle - previousAngle;
                        if (angleDiff <= planetGlyphMargin) {
                            // Move the current glyph out of the way of the next glyph
                            distance += distanceChangeAmount;
                            lineMultiplyer = 4
                        }
                    }
                }

                previousDistance = distance
                previousAngle = planetDetails.angle
                
                // drawPartialPolarLine(natalChart, canvas, Math.round(planetDetails.angle), natalChart.houseRingOuterRadius - (natalChart.planetGlyph.line * lineMultiplyer), natalChart.planetGlyph.line * lineMultiplyer, natalChart.signRingColor);
                drawCenteredText(natalChart, canvas, planetDetails.angle, distance, natalChart.signFont[planetDetails.planet.toLowerCase()], natalChart.planetGlyph.fontSize, natalChart.planetGlyph.color, 0, 'Astro', 'planet', planetDetails.planet, title);

                // drawCenteredText(natalChart, canvas, planetDetails.angle, natalChart.planetGlyphDistance-100, natalChart.planets[planetDetails.planet].deg.toString(), natalChart.acMcGlyphFontSize, '#000000', 0, 'Arial');
                // drawCenteredText(natalChart, canvas, planetDetails.angle, natalChart.planetGlyphDistance-30, natalChart.planets[planetDetails.planet].sign.toLowerCase(), natalChart.acMcGlyphFontSize, '#000000', 0, 'Arial');
                // drawCenteredText(natalChart, canvas, planetDetails.angle, natalChart.planetGlyphDistance-60, natalChart.planets[planetDetails.planet].min.toString(), natalChart.acMcGlyphFontSize, '#000000', 0, 'Arial');
            }
        })

        canvas.forEachObject(function (object) {
            object.selectable = false;
        });

        //устанавливаем правильное состояние аспектов
        setAspectsDependOfSettings(canvas, natalChart);
        setBackgroundInnerDependOfSettings(canvas, natalChart);

        $("#disableBackground").on("change", function () {
            setBackgroundInnerDependOfSettings(canvas, natalChart);
        });
        //скрываем линии аспектов при клике на чекбокс в настройках
        $("#disableAspects").on("change", function () {
            setAspectsDependOfSettings(canvas, natalChart);
        });

        // сохранение канваса в виде изображения png на сервере (делаем задержку для прогрузки изображений
        // иконок на канвасе, иначе они не сохранятюся в изображении)
        // добавление в сессию контена страницы для использования в печати pdf и отправки на почту
        //setTimeout(function () {
        saveImg();
        //}, 1000);

        canvas.on('mouse:over', overHandler);
        // canvas.on('mouse:down', overHandler);
    }

    function saveImg() {
        var resultPageContent = $('#report-container').clone(true);
        $(resultPageContent).find('.canvas-natal-cart').replaceWith('$$result_image$$');
        $(resultPageContent).find('.navbar').remove();
        // Remove the planet sign from the pdf
        $(resultPageContent).find('#planet-sign').remove();
        var dataUrl = canvas.toDataURL();
        // $("#imageContent").val(dataUrl);
        $("#pageContent").val(resultPageContent.html());
        $.ajax({
            type: "POST",
            url: "handler.php",
            dataType: 'JSON',
            data: {
                imgBase64: dataUrl,
                action: 'saveNatalChartImage'
            }
        }).done(function (backResponse) {
            var serverSavedImage = backResponse.image;
            // saving the name and image path to a variable
            natalChartImage = serverSavedImage;
            facebookUrl = 'http://www.facebook.com/'
                + 'sharer.php?u=' + encodeURIComponent(serverSavedImage)
                + '&title=' + encodeURIComponent('My Natal Chart')
                + '&picture=' + encodeURIComponent(serverSavedImage)
                + '&quote=' + encodeURIComponent('My Personalized Natal Chart');
            $('#facebook-share-link').attr('href', facebookUrl);
            twitterUrl = 'http://twitter.com/'
                + 'share?url=' + encodeURIComponent(serverSavedImage)
                + '&text=' + encodeURIComponent('My Natal Chart: ' + serverSavedImage);
            $('#twitter-share-link').attr('href', twitterUrl);
            $("#imageContent").val(serverSavedImage);
            $(".save-in-pdf-link").prop("disabled", false);
        });
    }

    function setAspectsDependOfSettings(canvas, natalChart) {
        if ($("#disableAspects").is(":checked")) {
            canvas.forEachObject(function (object) {
                if (object.hasOwnProperty('type') && object.type == 'aspect_lines') {
                    object.set({'stroke': 'transparent'});
                }
            });
            canvas.renderAll();
        } else {
            canvas.forEachObject(function (object) {
                if (object.hasOwnProperty('type') && object.type == 'aspect_lines') {
                    object.set({'stroke': natalChart.planetGlyph.color});
                }
            });
            canvas.renderAll();
        }
    }

    function setBackgroundInnerDependOfSettings(canvas, natalChart) {
        if ($("#disableBackground").is(":checked")) {
            canvas.setBackgroundImage(false);
            canvas.renderAll();
        } else {
            var imgURL = natalChart.userInfo.backgroundInner;
            // Create a new instance of the Image class
            var img = new Image();
            // When the image loads, set it as background image
            img.onload = function(imgD) {
                var f_img = new fabric.Image(img);
                var widtH = natalChart.houseRingInnerRadius*2 + 10;

                var scale = widtH / 451;//600 is the static img width
                canvas.setBackgroundImage(f_img, canvas.renderAll.bind(canvas), {
                    left: natalChart.chartWidth/2-widtH/2,
                    top: natalChart.chartHeight/2-widtH/2,
                    scaleX: scale,
                    scaleY: scale,
                    type: 'background',
                });

                canvas.renderAll();
            };
            img.src = imgURL;
        }
    }

    function setBackgroundDependOfSettings(img) {
        if ($("#disableBackground").is(":checked")) {
            $('#report-container').css("background-image", '');
            $('#report-container').css("background", 'rgb(235,235,235)');
        } else {
            $('#report-container').css("background", '');
            $('#report-container').css("background-image", 'url("' + img + '"');
        }
    }

    function isDate(txtDate) {
        var currVal = txtDate;
        if (currVal == '')
            return false;

        //Declare Regex
        var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[1];
        dtDay = dtArray[3];
        dtYear = dtArray[5];

        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    function drawCenteredText(natalChart, canvas, angle, radius, text, size, color, textAngle, fontFamily, type, content, title) {
        if (!color) color = '#000000';
        if (!textAngle) textAngle = 0;
        if (!fontFamily) fontFamily = 'Arial';

        var startX = natalChart.chartWidth / 2 + Math.sin(getTanDeg(angle)) * radius;
        var startY = natalChart.chartWidth / 2 - Math.cos(getTanDeg(angle)) * radius;
        var textWidth = getTextWidth(text, fontFamily, size);

        var params = {
            left: startX - textWidth / 2,
            top: startY - size / 2,
            width: textWidth,
            fill: color,
            fontSize: size,
            angle: textAngle,
            fontFamily: fontFamily,
        };
        var textObjject = new fabric.Text(text, params);
        if (type && content) {
            textObjject.set({'type': type});
            textObjject.set({'content': content});
            textObjject.set({'title': title});
        }
        if (text == 'D') {
            textObjject.set({'flipX': true});
        }

        canvas.add(textObjject);
    }

    function getTextWidth(text, fontFamily, size) {
        // re-use canvas object for better performance
        var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
        var context = canvas.getContext("2d");
        context.font = size + 'px ' + fontFamily;
        var metrics = context.measureText(text);
        return metrics.width;
    }

    /**
     * рисование линии на акружности
     * @param natalChart
     * @param canvas
     * @param angle
     * @param radius
     * @param length
     * @param color
     */
    function drawPartialPolarLine(natalChart, canvas, angle, radius, length, color) {
        if (!color) {
            color = '#ffffff'
        }

        var startX = natalChart.chartWidth / 2 + Math.sin(getTanDeg(angle)) * radius;
        var startY = natalChart.chartHeight / 2 - Math.cos(getTanDeg(angle)) * radius;

        var endX = natalChart.chartWidth / 2 + Math.sin(getTanDeg(angle)) * (radius + length);
        var endY = natalChart.chartHeight / 2 - Math.cos(getTanDeg(angle)) * (radius + length);

        var params = {stroke: color, strokeWidth: 1};
        canvas.add(new fabric.Line([startX, startY, endX, endY], params));
    }

    //преобразует градусы в радианы;
    function getTanDeg(deg) {
        return rad = deg * Math.PI / 180;
    }

    function convertDegArrayToDec(properties) {
        return parseFloat(properties.deg) + ((parseFloat(properties.min) + (parseFloat(properties.sec) / 60)) / 60);
    }

    $('#send-email-popup-open').magnificPopup({
        type: 'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    //отправить другу на почту
    $('#send-email-submit').on('click', function () {
        saveImg();
        if ($('#send-email-form').valid()) {
            $('#send-email-submit').text('Sending...').attr('disabled', 'disabled');

            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'handler.php',
                data: {'action': 'sendMail', 'email': $('#send-email-address').val(), 'imagePath': natalChartImage},
                success: function (callback) {
                    $('#send-email-address').val('');
                    $('#send-email-submit').text('Send').attr('disabled', false).blur();

                    noty({
                        layout: 'bottomCenter',
                        timeout: 3000,
                        progressBar: true,
                        killer: true,
                        theme: 'metroui',
                        text: 'Email successfully sent.<br>If you do not find the email in your inbox, please check your spam email folder.',
                        force: true,
                        type: 'success',
                        animation: {
                            open: 'animated flipInX',
                            close: 'animated zoomOut',
                            easing: 'swing',
                            speed: 500
                        }
                    });
                }
            });
        }
    });

    //отправить себе на почту
    $('#send-email-open').on('click', function () {
        saveImg();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'handler.php',
            data: {'action': 'sendMail', 'imagePath': natalChartImage},
            success: function (callback) {
                if (callback.success == true) {
                    noty({
                        layout: 'bottomCenter',
                        timeout: 3000,
                        progressBar: true,
                        killer: true,
                        theme: 'metroui',
                        text: 'Email successfully sent.<br>If you do not find the email in your inbox, please check your spam email folder.',
                        force: true,
                        type: 'success',
                        animation: {
                            open: 'animated flipInX',
                            close: 'animated zoomOut',
                            easing: 'swing',
                            speed: 500
                        }
                    });
                } else if (callback.success == false && callback.reason == 'no-email') {
                    noty({
                        layout: 'bottomCenter',
                        timeout: 3000,
                        progressBar: true,
                        killer: true,
                        theme: 'metroui',
                        text: 'No Email address.<br>Go back to and enter Email.',
                        force: true,
                        type: 'error',
                        animation: {
                            open: 'animated flipInX',
                            close: 'animated zoomOut',
                            easing: 'swing',
                            speed: 500
                        }
                    });
                }
            }
        });
    });

    //наведение на планету
    var overHandler = function (event) {
        setTimeout(function () {
            if (event.target && event.target.hasOwnProperty('type') && event.target.type == 'planet' && event.target.hasOwnProperty('content') && event.target.hasOwnProperty('title')) {
                $('.tooltip-link').remove();
                content = event.target.content;
                title = event.target.title;
                var topPopoverPosition = event.e.clientY - 5;
                var leftPopoverPosition = event.e.clientX - 5;
                $('<a>', {
                    text: title,
                    href: '#' + content.toLowerCase(),
                    css: {
                        backgroundColor: '#fffffff2',
                        position: 'absolute',
                        border: '1px solid #2e302e',
                        padding: '5px',
                        cursor: 'context-menu',
                        color: '#2e302e',
                    },
                    offset: {
                        top: topPopoverPosition,
                        left: leftPopoverPosition,
                    },
                    class: 'tooltip-link',
                }).appendTo('#report-container');

            } else {
                $('.tooltip-link').remove();
            }
        }, 500);
    };

    $(".settings-close").on("click", function () {
        $(".settings-popup").hide();
    });
    $(".settings-button").on("click", function () {
        $(".settings-popup").show();
    });

    if (getUrlParameter('autosubmit')) {
        $('#submit-natal-chart-form').trigger('click')
    }

});

