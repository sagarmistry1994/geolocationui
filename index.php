<?php
date_default_timezone_set('UTC');
error_reporting(E_ALL & ~E_NOTICE);

$cookies = (isset($_COOKIE['anc-details'])) ? json_decode(base64_decode($_COOKIE['anc-details'], true)) : false;

if (isset($_POST) && $_POST['autosubmit'])
{
    $day = $_POST['birth_date_day'];
    $month = $_POST['birth_date_month'];
    $year = $_POST['birth_date_year'];
    $hour = $_POST['birth_time_hour'];
    $minute = $_POST['birth_time_minute'];
    $meridiem = $_POST['birth_time_meridiem'];
    $datetime = DateTime::createFromFormat(
        "d-m-Y\Th:i:A",
        "{$day}-{$month}-{$year}T{$hour}:{$minute}:{$meridiem}",
        new DateTimeZone('Etc/UTC')
    );

    $url = 'https://hooks.zapier.com/hooks/catch/5454769/obzrylk/';
    
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded",
            'method'  => 'POST',
            'content' => json_encode([
                "firstName" => $_POST['current_first_name'],
                "dob" => $datetime->format("Y-m-d\TH:i:s\Z"),
                "email" => $_POST['send_email_address'],
                "source" => "chart.astrology.tv"
            ]),
        ),
    );

    $context = stream_context_create( $options );
    
    //$result = file_get_contents( $url, false, $context );
}

require_once("views/chart-form.php") ?>
